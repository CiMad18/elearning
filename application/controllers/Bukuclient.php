<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Bukuclient extends CI_Controller { 

function __construct() {
    parent::__construct();
    if(!isset($_SESSION['userlog'])){
        redirect('login');
    }
}
public function index()
{
    $this->load->model('buku_model');
    $data['title'] = "Buku Pelajaran";
    $data['cover_buku'] = base_url()."assets/fotobuku/";
    $data['file_buku'] = base_url()."assets/filebuku/";
    $data['getbuku']=$this->buku_model->getallbuku();
    $data['userdata'] = array(
        'username' => $this->session->userdata('namapengguna')
    );
    $this->load->view('client/headerclient',$data);
    $this->load->view('client/v_bukuclient2');
    $this->load->view('client/footerclient');
}

public function download($filename = null){				
    
    $this->load->helper(array('url','download'));
    $file = urldecode('assets/filebuku/'.$filename);
    force_download($file,NULL);
}

}