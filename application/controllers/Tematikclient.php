<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Tematikclient extends CI_Controller { 

function __construct() {
    parent::__construct();
    if(!isset($_SESSION['userlog'])){
        redirect('login');
    }
}
public function index()
{
    $this->load->model('tema_model');
    $data['gettema']=$this->tema_model->getalltema();
    $data['gambar_tema'] = base_url()."assets/gambartema/";
    $data['title'] = "Tematik";
    $this->load->view('client/headerclient',$data);
    $this->load->view('client/v_tematikclient');
    $this->load->view('client/footerclient');
    
}

public function openpertema($tema=null){
    $this->load->model('kamus_model');
    $this->load->model('tema_model');
    $data['getvideo']=$this->kamus_model->getkatabytema($tema);
    $data['tema']=$this->tema_model->gettemabyid($tema)->nama_tema;
    $data['kamus_url'] = base_url()."assets/kamus/";
    $data['thumbnail_url'] = $data['kamus_url']."thumbnail/";
    $data['title'] = "Tematik";
    $this->load->view('client/headerclient',$data);
    $this->load->view('client/v_pertematik');
    $this->load->view('client/footerclient');

}

}