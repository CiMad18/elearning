<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller { 
public function __construct()
{
    parent::__construct();
    if(!isset($_SESSION['userlog'])){
        redirect('login');
    }
    if ($this->session->userdata('rolele')!='Admin') {
        redirect('bukuclient');
    }
}
public function index()
{
    $data['title'] = "Dashboard";
    $data['userdata'] = array(
        'username' => $this->session->userdata('namapengguna')
    );
    
    $this->load->model('buku_model');
    $data['x']=$this->buku_model->ambilrowcount();	

    $this->load->model('pengguna_model');
    $data['y']=$this->pengguna_model->ambilrowcount();

    $this->load->model('kamus_model');
    $data['z']=$this->kamus_model->ambilrowcount();

    $this->load->view('admin/header', $data);
    $this->load->view('admin/sidebar');
    $this->load->view('admin/v_dashboard');
    $this->load->view('admin/footer');
}
}