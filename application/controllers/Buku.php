<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Buku extends CI_Controller { 

function __construct(){
    parent::__construct();
    $this->load->helper(array('url','download'));
    if(!isset($_SESSION['userlog'])){
        redirect('login');
    }
    if ($this->session->userdata('rolele')=='admin') {
        redirect('buku');			
    }
}

public function index()
{
    $this->load->model('buku_model');
    $data['title'] = "Buku";
	$data['getbuku']=$this->buku_model->getallbuku();
	// $data['x']=$this->buku_model->ambilrowcount();
    $this->load->view('admin/header',$data);
    $this->load->view('admin/sidebar');
    $this->load->view('admin/v_buku');
    $this->load->view('admin/footer');
}

public function actionbuk()
{    
    $this->load->model('buku_model');
    echo $this->input->post('actionbuk');
    if($this->input->post('actionbuk') == "addbuk"){
        if (isset($_FILES['fotobuku']['name']) && isset($_FILES['filebuku']['name']) ) {
            $fotobukuname = $this->upload_fotobuku();
            $filebukuname = $this->upload_filebuku();
            // echo $fotobukuname.$filebukuname;
            if($fotobukuname != ""){
                if ($filebukuname != ""){
                    $result = $this->buku_model->addbuku($fotobukuname,$filebukuname);
                    if($result){
                        echo 'berhasil tambah data';
                        $this->session->set_flashdata("message", "success");
                    }else {
                        echo "gagal tambah data";
                        $this->session->set_flashdata("message", "error");
                    }
                }
            }
        }else {
            echo "tidak ada file";
        }
        
    } elseif ($this->input->post('actionbuk') == "editbuk") {
        $fotobukuname = null;
        $filebukuname = null;
        if (isset($_FILES['fotobuku']['name']) ) {
            $fotobukuname = $this->upload_fotobuku();
        }
        if (isset($_FILES['filebuku']['name']) ) {
            $filebukuname = $this->upload_filebuku();
        }
        $result = $this->buku_model->editbuku($fotobukuname,$filebukuname);
        // if($result){
        //     echo 'berhasil edit buku' ;
        // }else {
        //     echo 'gagal edit buku' ;
        // }
        echo $result.$filebukuname;
    } 
}

public function upload_fotobuku()
{
    unset($config);
    $configFotobuku['upload_path'] = 'assets/fotobuku';
    $configFotobuku['max_size'] = '600';
    $configFotobuku['allowed_types'] = 'jpg|png|jpeg';
    $configFotobuku['overwrite'] = FALSE;
    $configFotobuku['max_filename'] = 0;
    $configFotobuku['remove_spaces'] = FALSE;
    $fotobuku_name = $_FILES['fotobuku']['name'];
    $configFotobuku['file_name'] = $fotobuku_name;

    $this->load->library('upload', $configFotobuku);
    $this->upload->initialize($configFotobuku);
    // return $fotobuku_name[0];
    if(!$this->upload->do_upload('fotobuku')) {
        echo $this->upload->display_errors();
        return 0;
    }else{
        return $fotobuku_name;
    }
}

public function upload_filebuku()
{
    unset($config);
    $configFilebuku['upload_path'] = 'assets/filebuku';
    $configFilebuku['max_size'] = '60000000';
    $configFilebuku['allowed_types'] = 'pdf';
    $configFilebuku['overwrite'] = FALSE;
    $configFilebuku['max_filename'] = 0;
    $configFilebuku['remove_spaces'] = FALSE;
    $filebuku_name = $_FILES['filebuku']['name'];
    $configFilebuku['file_name'] = $filebuku_name;

    $this->load->library('upload', $configFilebuku);
    $this->upload->initialize($configFilebuku);
    // return $video_name[0];
    if(!$this->upload->do_upload('filebuku')) {
        echo $this->upload->display_errors();
        return 0;
    }else{
        return $filebuku_name;
    }
}

public function getbuku()
{
    $this->load->model('buku_model');
    $id = $this->input->post('id');
    $result = $this->buku_model->ambilidbuku($id);
    echo json_encode($result);
}

public function delete()
{
        $this->load->model('buku_model');
        $id = $this->input->post('id');
        $result = $this->buku_model->delbuku($id);
        echo $result;
        // echo 'test';
        // redirect('Kamus');     
}



// public function add()
// {
//    $this->load->model('buku_model');
//    $result = $this->buku_model->addbuku();
   
//    if ($result) {
//         redirect("Buku");
//    } else {
//        return 'Proses gagal';
//    }
}