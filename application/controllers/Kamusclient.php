<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Kamusclient extends CI_Controller { 
public function __construct()
{
    parent::__construct();
    if(!isset($_SESSION['userlog'])){
        redirect('login');
    }
    
    $this->load->helper('url');
    $this->load->model('kamus_model');  
    $this->load->library("pagination");
}  
public function index()
{
    $data['base_url'] = site_url('kamusclient/');
    $data['kategori'] = $this->kamus_model->getkategori();
    $data['total_rows'] = $this->kamus_model->totalpage();
    // $data['paging_kamus'] = $this->kamus_model->paging();
    $data['kamus_url'] = base_url()."assets/kamus/";
    $data['thumbnail_url'] = $data['kamus_url']."thumbnail/";
    $data['title'] = "Kamus";

    //ambil data keyword 
    if($this->input->post('submit')){
        echo $this->input->post('keyword');
    }

    // print_r($data['paging_kamus']);
    $this->load->view('client/headerclient', $data);
    $this->load->view('client/v_kamusclient');
    $this->load->view('client/footerclient');
}   

public function getData()
{
    $kategori = $this->input->post('kategori');
    $page = $this->input->post('page');
    $result = $this->kamus_model->setpage($kategori, $page);
    echo json_encode($result);
}

public function getSearch()
{
    $key = $this->input->post('key');
    $result = $this->kamus_model->getSearch($key);
    echo json_encode($result);
}

public function getPage()
{
    $kategori = $this->input->post('kategori');
    $result = $this->kamus_model->totalpage($kategori);
    echo $result;
}

public function setkategori()
{
    $kategori = $this->input->post('kategori');
    $page = $this->input->post('page');

}

public function getvideo()
{
    $id = $this->input->post('id');
}

}
