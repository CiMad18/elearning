<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Kalimat extends CI_Controller {
public function __construct()
{
    parent::__construct();
    if(!isset($_SESSION['userlog'])){
        redirect('login');
    }
    if ($this->session->userdata('rolele')!='Admin') {
        redirect('bukuclient');
    }
} 

public function index()
{
    $this->load->model('kalimat_model');
    $data['getkalimat']=$this->kalimat_model->getallkalimat();
    $data['tema'] = $this->kalimat_model->gettema();
    $data['title'] = "Kalimat";
    $this->load->view('admin/header',$data);
    $this->load->view('admin/sidebar');
    $this->load->view('admin/v_kalimat');
    $this->load->view('admin/footer');
}

public function action()
{    
    $this->load->model('kalimat_model');
    if($this->input->post('action') == "add"){
        if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') {
            $videoname = $this->upload_video();            
            $result = $this->kalimat_model->addkalimat($videoname);
            if($result){
                $this->session->set_flashdata("message", "success");
            }else {
                $this->session->set_flashdata("message", "error");
            }
        }else {
            echo "tidak ada file";
        }
    }elseif ($this->input->post('action') == "edit") {
        $videoname = null;
        if (isset($_FILES['video']['name'])) {
            $videoname = $this->upload_video();
        }
        $result = $this->kalimat_model->editkalimat($videoname);
    }
}

public function upload_video()
{
    unset($config);
    $configVideo['upload_path'] = 'assets/kalimat';
    $configVideo['max_size'] = '60000';
    $configVideo['allowed_types'] = 'mp4';
    $configVideo['overwrite'] = FALSE;
    $configVideo['remove_spaces'] = TRUE;
    $video_name = $_FILES['video']['name'];
    $configVideo['file_name'] = $video_name;

    $this->load->library('upload', $configVideo);
    $this->upload->initialize($configVideo);
    if(!$this->upload->do_upload('video')) {
        echo $this->upload->display_errors();
        return 0;
    }else{
        $videoDetails = $this->upload->data();
        $data['video_name']= $configVideo['file_name'];
        $data['video_detail'] = $videoDetails;
        return $video_name;
    }
}

public function getkalimat()
{
    $this->load->model('kalimat_model');
    $id = $this->input->post('id');
    $result = $this->kalimat_model->ambilkalimat($id);
    echo json_encode($result);
}

public function delete()
{
        $this->load->model('kalimat_model');
        $id = $this->input->post('id');
        $result = $this->kalimat_model->delkalimat($id);
        echo $result;    
}

}