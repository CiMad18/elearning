<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Gamesclient extends CI_Controller { 

    function __construct() {
        parent::__construct();
        if(!isset($_SESSION['userlog'])){
            redirect('login');
        }
        $this->load->model('pengguna_model');
        $this->load->model('tema_model');
        $this->load->model('games_model');
    }
    public function index()
    {
        $data['title'] = "Games";
        $data['img_path'] = base_url().'assets/gambartema/';
        $data['data']=$this->games_model->getgamesdata();
        $this->load->view('client/headerclient', $data);
        $this->load->view('client/v_games');
        $this->load->view('client/footerclient');
    }

    public function cekgamedata()
    {
        
    }

    public function pilihlevel($tema = null)
    {           
        if($tema == null){
            redirect(base_url('GamesClient'));
        }
        $data['title'] = "Games";
        $data['tema'] = $tema;
        $gamesdata=$this->games_model->getgamesbytema($tema)[0];
        $data['username'] = $_SESSION['namapengguna'];
        $data['level'] = $gamesdata['level'];
        $this->load->view('client/headerclient', $data);
        $this->load->view('client/v_pilihlevel');
        $this->load->view('client/footerclient');
    }
    
    public function games($tema = null,$level = null)
    {
        $data['title'] = "Games";
        $gamesdata=$this->games_model->getgamesbytema($tema)[0];
        $data['maxlvl'] = $gamesdata['level'];
        $data['maxscore'] = $gamesdata['score'];
        $temaid = $this->games_model->gettemaid($tema);
        $kata = $this->games_model->getpertanyaan($temaid,$level);
        if($level == 1){
            $katakalimat = 'kata';
            $video_katakalimat = 'video_kata';
        }else{
            $katakalimat = 'kalimat';
            $video_katakalimat = 'video_kalimat';
        }
        $numbers = range(0, count($kata)-1);
        $pertanyaan = array();
        $video = array();
        $jawaban = array(); 
        foreach ($kata as $value) {
            array_push($pertanyaan, $value[$katakalimat]);
            array_push($video, $value[$video_katakalimat]);
            shuffle($numbers);
            $jawaban2 = array();
            $set = array();
            $acak = range(0,3);
            shuffle($acak);
            foreach($acak as $acak){
                array_push($set,$acak);
            }
            $jawaban2[$set[0]] =  $value[$katakalimat];
            $count = 1;
            $counta = 1;
            do{
                if($value[$katakalimat] != $kata[$numbers[$count]][$katakalimat]){
                    $jawaban2[$set[$counta]] = $kata[$numbers[$count]][$katakalimat];
                    $count++;
                    $counta++;
                }else {
                    $count++;
                    $jawaban2[$set[$counta]] = $kata[$numbers[$count]][$katakalimat];
                    $count++;
                    $counta++;
                }
            }while($counta <=3);
            array_push($jawaban, $jawaban2);
        }
        $data['pertanyaan'] = $pertanyaan;
        $data['video'] = $video;
        $data['jawaban'] = $jawaban; 
        $data['level'] = $level;
        $data['tema'] = $tema;
        $data['temaid'] = $temaid;
        // echo json_encode($data);
        $this->load->view('client/v_games2',$data);
    }

    // public function gamesa($tema = null,$level = null)
    // {
    //     $data['title'] = "Games";
    //     $this->load->model('games_model');
    //     $tema = $this->games_model->gettemaid($tema);
    //     $kata = $this->games_model->getpertanyaan($tema,$level);
    //     $pilihan_jawab = $this->games_model->getpilihanjawab($tema,$level);
    //     // $this->load->view('client/v_games2', $data);
    //     // json_encode($kata);

    //     if($level == 1){
    //         $katakalimat = 'kata';
    //         $video_katakalimat = 'video_kata';
    //     }else{
    //         $katakalimat = 'kalimat';
    //         $video_katakalimat = 'video_kalimat';
    //     }

    //     $numbers = range(0, count($kata)-1);
    //     $numbersb = range(0, count($pilihan_jawab)-1);
    //     shuffle($numbers);
    //     shuffle($numbersb);
    //     $pertanyaan = array();
    //     $pilihan = array();
    //     $video = array();
    //     $jawaban = array(); 
    //     $count = 0;
    //     $countans = 0;
    //     $a = 0;
    //     foreach ($numbersb as $value){
    //         array_push($pilihan, $pilihan_jawab[$value][$katakalimat]);
    //         $a++;
    //         if($a>40) break;
    //     }
    //     foreach ($numbers as $value){
    //         array_push($pertanyaan,$kata[$value][$katakalimat]);
    //         array_push($video,$kata[$value][$video_katakalimat]);
    //         $count++;
    //         $jawaban2 = array();
    //         $set = array();
    //         $acak = range(0,3);
    //         shuffle($acak);
    //         foreach($acak as $acak){
    //             array_push($set,$acak);
    //         }
    //         $jawaban2[$set[0]] = $kata[$value][$katakalimat];

    //         for($i = 1;$i<=3;$i++){
    //             $jawaban2[$set[$i]] = $pilihan[$countans];
    //             $countans++;
    //         }
    //         // print_r($jawaban2);
    //         array_push($jawaban, $jawaban2);
    //         if($count == 10){
    //             break;
    //         }
    //     }
    //     print_r($pertanyaan);
    //     // print_r($jawaban);

    //     // $data['pertanyaan'] = $pertanyaan;
    //     // $data['video'] = $video;
    //     // $data['jawaban'] = $jawaban;
    //     // // echo json_encode($data);
    //     // $this->load->view('client/headerclient', $data);
    //     // $this->load->view('client/v_games2', $data);
    //     //$this->load->view('client/footerclient');

    // }

    public function updatelevel()
    {
        echo  $this->games_model->updatelevel();
    }
}