<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller { 

public function __construct()
{
    parent::__construct();
    $this->load->model('Register_model');
    $this->load->model('Tema_model');
    $this->load->model('Games_model');

} 

public function index()
{
    $this->load->view('client/v_register');
}

public function aksi_register()
{
    $result = $this->Register_model->tambahuserbaru(); 
    $this->addgamedata($result[0]['last_id']);
    redirect('Homeclient');
}

public function addgamedata($id_pengguna = null)
{
    $result = $this->Tema_model->getalltema();
    // echo json_encode($result);
    foreach($result as $data){
        $this->Games_model->addgamedata($id_pengguna, $data['id_tema']);
    }
}

}