<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Homeclient extends CI_Controller { 
public function index()
{
    $this->load->model('buku_model');
    $data['x']=$this->buku_model->ambilrowcount();	

    $this->load->model('pengguna_model');
    $data['y']=$this->pengguna_model->ambilrowcount();

    $this->load->model('kamus_model');
    $data['z']=$this->kamus_model->ambilrowcount();

    $this->load->view('client/headerhome',$data);
    $this->load->view('client/v_homeclient');
    $this->load->view('client/footerclient');
}
}