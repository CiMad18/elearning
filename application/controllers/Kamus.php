<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Kamus extends CI_Controller {
public function __construct()
{
    parent::__construct();
    if(!isset($_SESSION['userlog'])){
        redirect('login');
    }
    if ($this->session->userdata('rolele')!='Admin') {
        redirect('bukuclient');
    }
}  

public function index()
{
    $this->load->model('kamus_model');
    $data['getkata']=$this->kamus_model->getallkata();
    $data['tema'] = $this->kamus_model->gettema();
    // $data['x']=$this->buku_model->ambilrowcount();
    $data['title'] = "Kamus";
    $this->load->view('admin/header',$data);
    $this->load->view('admin/sidebar');
    $this->load->view('admin/v_kamus');
    $this->load->view('admin/footer');
}

public function action()
{    
    $this->load->model('kamus_model');
    // echo $this->input->post('action');
    if($this->input->post('action') == "add"){
        if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') {
            $videoname = $this->upload_video();  
            $result = $this->kamus_model->addkamus($videoname);
            if($result){
                echo "berhasil";
                $this->session->set_flashdata("message", "success");
            }else {
                echo "error";
                $this->session->set_flashdata("message", "error");
            }
        }else {
            echo "tidak ada file";
            $this->session->set_flashdata("message", "error");
        }
    }elseif ($this->input->post('action') == "edit") {
        $videoname = null;
        if (isset($_FILES['video']['name'])) {
            $videoname = $this->upload_video();  
        }
        $result = $this->kamus_model->editkata($videoname);
    } 
}

public function upload_video()
{
    unset($config);
    $configVideo['upload_path'] = 'assets/kamus';
    $configVideo['max_size'] = '60000';
    $configVideo['allowed_types'] = 'mp4';
    $configVideo['overwrite'] = TRUE;
    $configVideo['remove_spaces'] = TRUE;
    $video_name = $_FILES['video']['name'];
    $configVideo['file_name'] = $video_name;
    $this->load->library('upload', $configVideo);
    $this->upload->initialize($configVideo);
    // return $video_name[0];
    if(!$this->upload->do_upload('video')) {
        echo $this->upload->display_errors();
        return 0;
    }else{
        $videoDetails = $this->upload->data();
        $file_name= $videoDetails['file_name'];
        $raw_name = $videoDetails['raw_name'];
        $full_path = $videoDetails['full_path'];
        $file_path = $videoDetails['file_path']."thumbnail/";
        $exec = "ffmpeg -i ".$full_path." -vframes 1 -s 600x350 ".$file_path.$raw_name.".jpeg"." -y";
        // exec($exec);
        // echo $exec;

        return $file_name;
    }
}

public function makethumbnail()
{
    $this->load->model('kamus_model');
    $result = $this->kamus_model->getallkata();
    $file_path = realpath($_SERVER["DOCUMENT_ROOT"])."/elearning/assets/kamus/";
    echo $file_path;
    foreach ($result as $r) {
        $name = preg_replace('/\\.[^.\\s]{3,4}$/', '' ,$r['video_kata']);
        $exec = "ffmpeg -i ".$file_path.$r['video_kata']." -vframes 1 -s 600x350 ".$file_path."thumbnail/".$name.".jpeg"." -y";
        // exec($exec);
        // echo $exec;
    }
}

public function getkata()
{
    $this->load->model('kamus_model');
    $id = $this->input->post('id');
    $result = $this->kamus_model->ambilkata($id);
    echo json_encode($result);
}

public function delete()
{
        $this->load->model('kamus_model');
        $id = $this->input->post('id');
        $result = $this->kamus_model->delkata($id);
        echo $result;
        // echo 'test';
        // redirect('Kamus');     
}
}