<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna extends CI_Controller {
public function __construct()
{
    parent::__construct();
    if(!isset($_SESSION['userlog'])){
        redirect('login');
    }
    if ($this->session->userdata('rolele')!='Admin') {
        redirect('bukuclient');
    }
    $this->load->model('pengguna_model');
    $this->load->model('tema_model');
    $this->load->model('games_model');

} 

public function index()
{
    $data['getuser']=$this->pengguna_model->getalluser();
    // $data['x']=$this->buku_model->ambilrowcount();
    $data['title'] = "Pengguna";
    $this->load->view('admin/header',$data);
    $this->load->view('admin/sidebar');
    $this->load->view('admin/v_pengguna');
    $this->load->view('admin/footer');
}

public function action()
{    
    if($this->input->post('action') == "add"){
        if (isset($_FILES['foto_profile']['name']) ) {
            $foto_profile = $this->upload_fotoprofile();
            if($foto_profile != ""){
                $result = $this->pengguna_model->adduser($foto_profile);
                $this->addgamedata($result[0]['last_id']);
                $this->session->set_flashdata("message", "success");
            }
        }else {
            echo "tidak ada file";
            $this->session->set_flashdata("message", "error");
        }
        
    } elseif ($this->input->post('action') == "edit") {
        $foto_profile = null;
        if (isset($_FILES['foto_profile']['name']) ) {
            $foto_profile = $this->upload_fotoprofile();
        }
        $result = $this->pengguna_model->edituser($foto_profile);
        // if($result){
        //     echo 'berhasil edit buku' ;
        // }else {
        //     echo 'gagal edit buku' ;
        // }
        echo $result.$foto_profile;
    } 
}

public function addgamedata($id_pengguna = null)
{
    $result = $this->tema_model->getalltema();
    echo json_encode($result);
    foreach($result as $data){
        $this->games_model->addgamedata($id_pengguna, $data['id_tema']);
    }
}

public function upload_fotoprofile()
{
    unset($config);
    $configFotobuku['upload_path'] = 'assets/fotoprofile';
    $configFotobuku['max_size'] = '600';
    $configFotobuku['allowed_types'] = 'jpg|png|jpeg';
    $configFotobuku['overwrite'] = FALSE;
    $configFotobuku['max_filename'] = 0;
    $configFotobuku['remove_spaces'] = FALSE;
    $fotoprofile_name = $_FILES['foto_profile']['name'];
    $configFotobuku['file_name'] = $fotoprofile_name;

    $this->load->library('upload', $configFotobuku);
    $this->upload->initialize($configFotobuku);
    // return $fotoprofile_name[0];
    if(!$this->upload->do_upload('foto_profile')) {
        echo $this->upload->display_errors();
        return 0;
    }else{
        return $fotoprofile_name;
    } 
}
 
public function getuser()
{
    $id = $this->input->post('id');
    $result = $this->pengguna_model->ambilidpengguna($id);
    echo json_encode($result);
}

public function delete()
{
    $result = $this->pengguna_model->deluser();
    print_r($result);
}

}