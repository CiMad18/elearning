<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller { 

public function index()
{
    $this->load->view('client/v_loginclient');
}

public function aksi_login()
    {
    	$this->load->model('login_model'); 
    	$user = $this->input->post('username');
    	$pass = $this->input->post('password');
    	$row = $this->login_model->ambillogin()->num_rows();
    	if (!empty($user) && !empty($pass)) {
    		if ($row==1) {
    			$ambil = $this->login_model->ambillogin()->row();
    			$data = array(
    				"userlog" => $ambil->id_pengguna,
    		  		"namapengguna" => $ambil->nama_pengguna,
    		  		"nama_lengkap" => $ambil->nama_lengkap,
    				"nohp" => $ambil->no_hp,
    				"password" => $ambil->password,
    				"fotoprofile" => $ambil->foto_profile,
    				"rolele" => $ambil->role,
    				"logged" => TRUE
    			);
    			$this->session->set_userdata($data);
    			redirect('dashboard');
                
    		}else {
    			$this->session->set_flashdata("message","username / password salah");
    			redirect('login');	
    		} 
    	} else {
    		$this->session->set_flashdata("message","username / password salah");
    		redirect('login','refresh');
    	}

		}
}