<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Tema extends CI_Controller {
public function __construct()
{
    parent::__construct();
    if(!isset($_SESSION['userlog'])){
        redirect('login');
    }
    if ($this->session->userdata('rolele')!='Admin') {
        redirect('bukuclient');
    }
}

public function index()
{
    $this->load->model('tema_model');
    $data['gettema']=$this->tema_model->getalltema();
    // $data['x']=$this->buku_model->ambilrowcount();
    $data['title'] = "Tema";
    $this->load->view('admin/header',$data);
    $this->load->view('admin/sidebar');
    $this->load->view('admin/v_tema');
    $this->load->view('admin/footer');
}

public function action()
{    
    $this->load->model('tema_model');
    echo $this->input->post('action');
    if($this->input->post('action') == "add"){
        if (isset($_FILES['gambar_tema']['name']) ) {
            $gambar_tema = $this->upload_gambartema();
            // echo $fotobukuname.$filebukuname;
            if($gambar_tema != ""){
                    $result = $this->tema_model->addtema($gambar_tema);
                    if($result){
                        echo 'berhasil tambah data';
                        $this->session->set_flashdata("message", "success");
                    }else {
                        echo "gagal tambah data";
                        $this->session->set_flashdata("message", "error");
                    
                }
            }
        }else {
            echo "tidak ada file";
        }
        
    } elseif ($this->input->post('action') == "edit") {
        $gambar_tema = null;
        if (isset($_FILES['gambar_tema']['name']) ) {
            $gambar_tema = $this->upload_gambartema();
        }
        $result = $this->pengguna_model->edittema($gambar_tema);
        // if($result){
        //     echo 'berhasil edit buku' ;
        // }else {
        //     echo 'gagal edit buku' ;
        // }
        echo $result.$gambar_tema;
    } 
}

public function upload_gambartema()
{
    unset($config);
    $configGambartema['upload_path'] = 'assets/gambartema';
    $configGambartema['max_size'] = '600';
    $configGambartema['allowed_types'] = 'jpg|png|jpeg';
    $configGambartema['overwrite'] = FALSE;
    $configGambartema['max_filename'] = 0;
    $configGambartema['remove_spaces'] = FALSE;
    $gambartema_name = $_FILES['gambar_tema']['name'];
    $configGambartema['file_name'] = $gambartema_name;

    $this->load->library('upload', $configGambartema);
    $this->upload->initialize($configGambartema);
    // return $fotoprofile_name[0];
    if(!$this->upload->do_upload('gambar_tema')) {
        echo $this->upload->display_errors();
        return 0;
    }else{
        return $gambartema_name;
    }
}

public function gettema()
{
    $this->load->model('tema_model');
    $id = $this->input->post('id');
    $result = $this->tema_model->ambilidtema($id);
    echo json_encode($result);
}

public function delete()
{
        $this->load->model('tema_model');
        $result = $this->tema_model->deltema();
        print_r($result);
}

}