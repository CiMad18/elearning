  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tema</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tema</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    
    <section class="content">
        <div class="container-fluid">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-lg" onclick="add()">Tambah Data</button>
        </div><br>
        <div class="container-fluid">
        <?php if($this->session->flashdata("message") == "success"){ ?>
        <div class="alert alert-success" role="alert">
          Success Tambah Tema.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata("message") == "error"){ ?>
        <div class="alert alert-danger" role="alert">
          Gagal Tambah Tema.
        </div>
        <?php } ?>
        <div class="card">
    <!-- /.card-header -->
    <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <td>No</td>
            <td>Nama Tema</td>
            <td>Gambar Tema</td>
            <td>Aksi</td>
        </tr>
        </thead>
        <tbody>
        <?php
            $i=1;
              // var_dump($allmhs);
              if(!empty($gettema)){
                  foreach ($gettema as $gt){
                      echo ' 
                      <tr>
                      <td>'.$i.'</td>
                      <td>'.$gt['nama_tema'].'</td>
                      <td>'.$gt['gambar_tema'].'</td>
                      <td>
                          <button type="button" class="btn btn-info" onclick="edit('.$gt['id_tema'].')" data-toggle="modal" data-target="#modal-lg">Edit</button>
                          <button type="button" class="btn btn-danger" onclick="hapus('.$gt['id_tema'].')" data-toggle="modal" data-target="#modal-danger"><i class="fas fa-trash-alt"></i></button>
                      </td>
                      </tr>';
                      $i+=1;
                  }
              }else{
                  echo 
                  '<tr>
                  <td colspan="4" align="center">Tidak ada data</td>
                  </tr>';
              }
          ?>
        </tbody>
        <tfoot>
        <tr>
            <td>No</td>
            <td>Nama Tema</td>
            <td>Gambar Tema</td>
            <td>Aksi</td>
        </tr>
        </tfoot>
    </table>
    </div>
    <!-- /.card-body -->
    </div>
</div>
            <!-- /.content-header -->
            <!-- Main content -->
    </div>
    </section>
    <!-- modal tambah pengguna -->
    <div class="modal fade" id="modal-lg">
      <div class="overlay"></div>
        <div class="spanner">
        <div class="loader"></div>
        <p>Menambahkan Tema</p>
      </div>
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="form_data" name="form_data" enctype="multipart/form-data">
                    <input type="hidden" id="action" name="action" value="add">
                    <input type="hidden" id="id_tema" name="id_tema">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Pengguna</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Tema</label>
                            <input type="text" id="nama_tema" name="nama_tema" class="form-control" id="exampleInputEmail1" placeholder="Masukkan nama tema">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Gambar Tema</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input onchange="readURL(this)" type="file" class="custom-file-input" id="gambar_tema" name="gambar_tema">
                                    <label class="custom-file-label" for="exampleInputFile" id="labeltema">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <img id="previmg" width="50%" src="" />
                    </div>
                    </form>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onclick="submit()" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
    </div>

    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog" >
          <div class="modal-content bg-danger">
            <div class="modal-header">
            <input type="hidden" id="tema" >
              <h4 class="modal-title">Hapus Tema</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Yakin ingin menghapus tema ini ?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
              <button type="button" class="btn btn-outline-light" onclick ="hapustema()">Ya</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    </div>

<script>
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function(e) {
          $('#previmg').attr('src', e.target.result);
        }
        
        reader.readAsDataURL(input.files[0]); // convert to base64 string
      }
    }
    function add() {
        $('#action').val('add');
        $('#id_pengguna').val('');
        $('#nama_tema').val('');   
        $('#labeltema').html('Choose file');
    }
    
    function edit(id) {
        $('#action').val('edit');
        $.ajax({
            url : "<?= base_url('tema/gettema') ?>",
            type : 'post',
            dataType : 'json',
            data : {
                id: id 
            },
            success : function (data){
                $('#action').val('edit');
                $('#id_pengguna').val(data[0].id_pengguna);
                $('#nama_tema').val(data[0].nama_tema);
                $('#labeltema').html(data[0].gambar_tema);
                $('#previmg').attr('src', '<?= base_url("assets/gambartema/")?>'+data[0].gambar_tema);
            },
            error : function(){
            }
        })    
    }

    function submit(){
      $("div.spanner").addClass("show");
      $("div.overlay").addClass("show");
        var data = new FormData();
        //Form data
        var form_data = $('#form_data').serializeArray();
        $.each(form_data, function (key, input) {
            data.append(input.name, input.value);
        });

        //File data
        var file_data = $('input[name="gambar_tema"]')[0].files;
        data.append("gambar_tema", file_data[0]);
        // alert(data);
        // var data = $('#form_data').serialize();
        $.ajax({
            url : "<?= base_url('tema/action') ?>",
            method: "post",
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
              // console.log(data)
            $("div.spanner").removeClass("show");
            $("div.overlay").removeClass("show");
            location.reload();
            },
            error: function (e) {
            console.log(JSON.stringify(e));
            $("div.spanner").removeClass("show");
            $("div.overlay").removeClass("show");             
            }
        })
    }

    function hapustema(){
        var id = $('#tema').val();
        $.ajax({
            url : "<?= base_url('tema/delete') ?>",
            method : 'post',
            data : {
                id: id
            },success : function(data){
                // console.log(data);
                location.reload();
            }
        })
    }

    function hapus(id){
        $('#tema').val(id);
        
    } 
    
    var myVar;

    function myFunction() {
  myVar = setTimeout(showPage, 300);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}
</script>