  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" >
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Buku Pelajaran</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Buku Pelajaran</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <section class="content">
        <div class="container-fluid">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-lg" onclick="add()">Tambah Data</button>
        </div>
        <br>
        <?php if($this->session->flashdata("message") == "success"){ ?>
        <div class="alert alert-success" role="alert">
          Success Tambah Buku.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata("message") == "error"){ ?>
        <div class="alert alert-danger" role="alert">
          Gagal Tambah Buku.
        </div>
        <?php } ?>
        <div class="card">
    <!-- /.card-header -->
    <div class="card-body">
    <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
            <td>No</td>
            <td>Nama Buku</td>
            <td>Foto Buku</td>
            <td>File Buku</td>
            <td>Aksi</td>
        </tr>
        </thead>
        <tbody> 
        <?php
              $i=1;
                  // var_dump($allmhs);
                  if(!empty($getbuku)){
                  foreach ($getbuku as $gb){
                      echo ' 
                      <tr>
                      <td>'.$i.'</td>
                      <td>'.$gb['nama_buku'].'</td>
                      <td>'.$gb['foto_buku'].'</td>
                      <td>'.$gb['file_buku'].'</td>
                      <td>
                          <button type="button" class="btn btn-info" onclick="edit('.$gb['id_buku'].')" data-toggle="modal" data-target="#modal-lg">Edit</button>
                          <button type="button" class="btn btn-danger" onclick="hapus('.$gb['id_buku'].')" data-toggle="modal" data-target="#modal-danger"><i class="fas fa-trash-alt"></i></button>
                      </td>
              </tr>';
              $i+=1;
                }
                  } else {
                    echo 
                        '<tr>
                        <td colspan="5" align="center">Tidak ada data</td>
                        </tr>';
                  }
              ?>
        </tbody>
        <tfoot>
        <tr>
        <td>No</td>
        <td>Nama Buku</td>
        <td>Foto Buku</td>
        <td>File Buku</td>
        <td>Aksi</td>
        </tr>
        </tfoot>
    </table>
    </div>
    <!-- /.card-body -->
    </div>
</div>
    </section>
  </div>

  <div class="modal fade" id="modal-lg">
    <div class="overlay"></div>
    <div class="spanner">
      <div class="loader"></div>
      <p>Uploading Buku Pelajaran</p>
    </div>
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <form id="form_databuku" name="form_databuku" enctype="multipart/form-data">
          <input type="hidden" id="actionbuk" name="actionbuk" value="addbuk">
          <input type="hidden" id="id_buku" name="id_buku">
          <!-- <form action="buku/add" method="POST" enctype="multipart/form-data"> -->
          <div class="modal-header">
              <h4 class="modal-title">Buku Pelajaran</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="form-group">
                  <label for="exampleInputEmail1">Nama Buku</label>
                  <input type="text" class="form-control" id="nambuk" name="nambuk" placeholder="Masukkan Judul Buku Pelajaran">
              </div>
              <div class="form-group">
                  <label for="exampleInputFile">Cover</label>
                  <div class="input-group">
                      <div class="custom-file">
                          <input type="file" class="custom-file-input" id="fotobuku" name="fotobuku">
                          <label class="custom-file-label" id="labelfoto" for="exampleInputFile">Choose file</label>
                      </div>
                  </div>
              </div>
              <div class="form-group">
                  <label for="exampleInputFile">File Buku</label>
                  <div class="input-group">
                      <div class="custom-file">
                          <input type="file" class="custom-file-input" id="filebuku" name="filebuku">
                          <label class="custom-file-label" id="labelfile" for="exampleInputFile">Choose file</label>
                      </div>
                  </div>
              </div>
          </div>
          </form>
          <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" onclick="submitbuku()" class="btn btn-primary">Save changes</button>
          </div> 
      </div>
          <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" id="modal-danger">
      <div class="modal-dialog">
        <div class="modal-content bg-danger">
            <div class="modal-header">
            <input type="hidden" id="bukuku" >
              <h4 class="modal-title">Hapus Buku</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Ingin menghapus buku ini ?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
              <button type="button" class="btn btn-outline-light" onclick="hapusbuku()">Ya</button>
            </div>
          </div>
        <!-- /.modal-content -->
        </div>
      <!-- /.modal-dialog -->
      </div>
  </div>

  <script>
    function add() {
      $('#actionbuk').val('addbuk'); 
      $('#nambuk').val('');
      $('#id_buku').val('');     
      $('#labelfoto').html('Choose file');
      $('#labelfile').html('Choose file');
    }
    function edit(id) {
      $('#actionbuk').val('editbuk');
      // alert(id);
        $.ajax({
            url : "<?= base_url('buku/getbuku') ?>",
            type : 'post',
            dataType : 'json',
            data : {
                id: id
            },
            success : function (data){
                // alert(data[0].id_kamus);
                // console.log(data);
                $('#id_buku').val(data[0].id_buku);
                $('#nambuk').val(data[0].nama_buku);
                $('#labelfoto').html(data[0].foto_buku);
                $('#labelfile').html(data[0].file_buku);
            },
            error : function(){
            }
        })    
    }
 
    function submitbuku(){
      $("div.spanner").addClass("show");
      $("div.overlay").addClass("show");
        var data = new FormData();
        //Form data
        var form_databuku = $('#form_databuku').serializeArray();
        $.each(form_databuku, function (key, input) {
            data.append(input.name, input.value);
        });

        //File data
        var file_fotobuku = $('input[name="fotobuku"]')[0].files;
        data.append("fotobuku", file_fotobuku[0]);

        var file_bukuku = $('input[name="filebuku"]')[0].files;
        data.append("filebuku", file_bukuku[0]);
        // alert(data);
        // var data = $('#form_data').serialize();
        $.ajax({
            url : "<?= base_url('buku/actionbuk') ?>",
            method: "post",
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
              // alert(data);
            $("div.spanner").removeClass("show");
            $("div.overlay").removeClass("show");
            location.reload();
            },
            error: function (e) {
              console.log(JSON.stringify(e));
              $("div.spanner").removeClass("show");
              $("div.overlay").removeClass("show");
            }
        })
    }

    function hapusbuku(){
        var id = $('#bukuku').val();
        $.ajax({
            url : "<?= base_url('buku/delete') ?>",
            method : 'post',
            data : {
                id: id
            },success : function(data){
                // alert(data)
                location.reload();
            }
        }) 
    }

    function hapus(id){
        $('#bukuku').val(id);
        
    } 

var myVar;

function myFunction() {
  myVar = setTimeout(showPage, 300);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}
</script>


  