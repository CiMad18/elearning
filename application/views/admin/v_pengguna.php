  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pengguna</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Pengguna</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    
    <section class="content">
        <div class="container-fluid">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-lg" onclick="add()">Tambah Data</button>
        </div><br>
        <div class="container-fluid">
        <?php if($this->session->flashdata("message") == "success"){ ?>
        <div class="alert alert-success" role="alert">
          Success Tambah Pengguna.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata("message") == "error"){ ?>
        <div class="alert alert-danger" role="alert">
          Gagal Tambah Pengguna.
        </div>
        <?php } ?>
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <td>No</td>
                <td>Nama Pengguna</td>
                <td>Nama Lengkap</td>
                <td>No Handphone</td>
                <td>Password</td>
                <td>Foto Profile</td>
                <td>Role</td>
                <td>Aksi</td>
                </tr>
                </thead>
                <tbody>
                <?php
                $i=1;
                    // var_dump($allmhs);
                    if(!empty($getuser)){
                        foreach ($getuser as $gu){
                            echo ' 
                            <tr>
                            <td>'.$i.'</td>
                            <td>'.$gu['nama_pengguna'].'</td>
                            <td>'.$gu['nama_lengkap'].'</td>
                            <td>'.$gu['no_hp'].'</td>
                            <td>'.$gu['password'].'</td>
                            <td>'.$gu['foto_profile'].'</td>
                            <td>'.$gu['role'].'</td>
                            <td>
                                <button type="button" class="btn btn-info" onclick="edit('.$gu['id_pengguna'].')" data-toggle="modal" data-target="#modal-lg">Edit</button>
                                <button type="button" class="btn btn-danger" onclick="hapus('.$gu['id_pengguna'].')" data-toggle="modal" data-target="#modal-danger"><i class="fas fa-trash-alt"></i></button>
                            </td>
                            </tr>';
                            $i+=1;
                        }
                    }else{
                        echo 
                        '<tr>
                        <td colspan="8" align="center">Tidak ada data</td>
                        </tr>';
                    }
                ?>
                </tbody>
                <tfoot>
                <tr>
                    <td>No</td>
                    <td>Nama Pengguna</td>
                    <td>Nama Lengkap</td>
                    <td>No Handphone</td>
                    <td>Password</td>
                    <td>Foto Profile</td>
                    <td>Role</td>
                    <td>Aksi</td>
                </tr>
                </tfoot>
            </table>
            </div>
            <!-- /.card-body -->
            </div>
        </div>
            <!-- /.content-header -->
            <!-- Main content -->
        </div>
    </section>
    <!-- modal tambah pengguna -->
    <div class="modal fade" id="modal-lg">
      <div class="overlay"></div>
        <div class="spanner">
        <div class="loader"></div>
        <p>Menambahkan pengguna</p>
      </div>
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="form_data" name="form_data" enctype="multipart/form-data">
                    <input type="hidden" id="action" name="action" value="add">
                    <input type="hidden" id="id_pengguna" name="id_pengguna">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Pengguna</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Pengguna</label>
                            <input type="text" id="pengguna" name="pengguna" class="form-control" id="exampleInputEmail1" placeholder="Masukkan nama pengguna">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Lengkap</label>
                            <input type="text" id="nama_lengkap" name="nama_lengkap" class="form-control" id="exampleInputEmail1" placeholder="Masukkan nama lengkap">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nomor Handphone</label>
                            <input type="text" id="no_hp" name="no_hp" class="form-control" id="exampleInputEmail1" placeholder="Masukkan nomor handphone">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Password</label>
                            <input type="password" id="password" name="password" class="form-control" id="exampleInputEmail1" placeholder="Masukkan password">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Foto Profile</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="foto_profile" name="foto_profile">
                                    <label class="custom-file-label" for="exampleInputFile" id="labelfoto">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                        <label>Select Role</label>
                        <select class="form-control" id ="role" name="role">
                          <option value = "Admin">Admin</option>
                          <option value = "User">User</option>
                        </select>
                      </div>
                    </div>
                    </form>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onclick="submit()" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
    </div>

    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog" >
          <div class="modal-content bg-danger">
            <div class="modal-header">
            <input type="hidden" id="kalimat" >
              <h4 class="modal-title">Hapus Pengguna</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Yakin ingin menghapus pengguna ini ?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
              <button type="button" class="btn btn-outline-light" onclick ="hapuspengguna()">Ya</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

    </div>

<script>
    function add() {
        $('#action').val('add');
        $('#id_pengguna').val('');
        $('#pengguna').val('');
        $('#nama_lengkap').val('');
        $('#no_hp').val('');  
        $('#password').val('');     
        $('#labelfoto').html('Choose file');
        $('#role').val('');     
    }
    
    function edit(id) {
        $('#action').val('edit');
        $.ajax({
            url : "<?= base_url('pengguna/getuser') ?>",
            type : 'post',
            dataType : 'json',
            data : {
                id: id
            },
            success : function (data){
                $('#action').val('edit');
                $('#id_pengguna').val(data[0].id_pengguna);
                $('#pengguna').val(data[0].nama_pengguna);
                $('#nama_lengkap').val(data[0].nama_lengkap);
                $('#no_hp').val(data[0].no_hp);
                $('#password').val(data[0].password);
                $('#labelfoto').html(data[0].foto_profile);
                $('#role').val(data[0].role);  
            },
            error : function(){
            }
        })    
    }

    function submit(){
      $("div.spanner").addClass("show");
      $("div.overlay").addClass("show");
        var data = new FormData();
        //Form data
        var form_data = $('#form_data').serializeArray();
        $.each(form_data, function (key, input) {
            data.append(input.name, input.value);
        });

        //File data
        var file_data = $('input[name="foto_profile"]')[0].files;
        data.append("foto_profile", file_data[0]);
        // alert(data);
        // var data = $('#form_data').serialize();
        $.ajax({
            url : "<?= base_url('pengguna/action') ?>",
            method: "post",
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
              console.log(data)
            $("div.spanner").removeClass("show");
            $("div.overlay").removeClass("show");
            location.reload();
            },
            error: function (e) {
            console.log(JSON.stringify(e));
            $("div.spanner").removeClass("show");
            $("div.overlay").removeClass("show");             
            }
        })
    }

    function hapuspengguna(){
        var id = $('#pengguna').val();
        $.ajax({
            url : "<?= base_url('pengguna/delete') ?>",
            method : 'post',
            data : {
                id: id
            },success : function(data){
                // console.log(data);
                location.reload();
            }
        })
    }

    function hapus(id){
        $('#pengguna').val(id);
        
    } 
    
    var myVar;

    function myFunction() {
  myVar = setTimeout(showPage, 300);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}
</script>