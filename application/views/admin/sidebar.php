
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <!-- Sidebar -->
    <div class="sidebar"> 
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?= base_url().'assets/fotoprofile/'.$_SESSION['fotoprofile']?>" class="img-circle" style="width: 75px; height: 80px;" alt="User Image">
        </div>
        <div class="info">
          <span style="font-size: medium; color:white;"><?= $_SESSION['namapengguna']?></span><br>
          <span style="color: cornsilk;">Administrator</span>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <ul class="nav nav-treeview"> 
            <li class="nav-item">
                <a href="<?php base_url();?>dashboard" class="nav-link <?php if($title == "Dashboard")echo "active"; ?>">
                  <i class="fas fa-house-user nav-icon"></i>
                  <p>Dashboard</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php base_url();?>Pengguna" class="nav-link <?php if($title == "Pengguna")echo "active"; ?>">
                  <i class="fas fa-portrait nav-icon"></i>
                  <p>Pengguna</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php base_url();?>Buku" class="nav-link <?php if($title == "Buku")echo "active"; ?>">
                  <i class="fas fa-book-open nav-icon"></i>
                  <p>Buku Pelajaran</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?>Kamus" class="nav-link <?php if($title == "Kamus")echo "active"; ?>">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kamus</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?>Kalimat" class="nav-link <?php if($title == "Kalimat")echo "active"; ?>">
                  <i class="fa fa-align-left nav-icon"></i>
                  <p>Kalimat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url();?>Tema" class="nav-link <?php if($title == "Tema")echo "active"; ?>">
                  <i class="fas fa-star nav-icon"></i>
                  <p>Tema</p>
                </a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>