  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Kamus</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Kamus</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    
    <section class="content">
        <div class="container-fluid">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-lg" onclick="add()">Tambah Data</button>
        </div><br>
        <div class="container-fluid">
        <?php if($this->session->flashdata("message") == "success"){ ?>
        <div class="alert alert-success" role="alert">
          Success Tambah Kamus.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata("message") == "error"){ ?>
        <div class="alert alert-danger" role="alert">
          Gagal Tambah kamus.
        </div>
        <?php } ?>
        <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                <th>No</th>
                <th>Kosakata</th>
                <th>Video</th>
                <th>Tema</th>
                <th>Aksi</th>
                </tr>
                </thead> 
                <tbody>
                <?php
                $i=1;
                    // var_dump($allmhs);
                    if(!empty($getkata)){
                        foreach ($getkata as $gk){
                            echo '   
                            <tr>
                                <td>'.$i.'</td>
                                <td>'.$gk['kata'].'</td>
                                <td>'.$gk['video_kata'].'</td>
                                <td>'.$gk['tema'].'</td>
                                <td>
                                    <button type="button" class="btn btn-info" onclick="edit('.$gk['id_kamus'].')" data-toggle="modal" data-target="#modal-lg">Edit</button>
                                    <button type="button" class="btn btn-danger" onclick ="hapus('.$gk['id_kamus'].')" data-toggle="modal" data-target="#modal-danger"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>';
                            $i+=1;
                        }
                    }else{
                        echo 
                        '<tr>
                        <td colspan="5" align="center">Tidak ada data</td>
                        </tr>';
                    }
                ?>
                </tbody>
                <tfoot>
                <tr>
                    <th>No</th>
                    <th>Kosakata</th>
                    <th>Video</th>
                    <th>Tema</th>
                    <th>Aksi</th>
                </tr>
                </tfoot>
            </table>
            </div>
            <!-- /.card-body -->
            </div>
        </div>
    </section>
    <!-- modal tambah kata -->
    <div class="modal fade" id="modal-lg">
    <div class="overlay"></div>
        <div class="spanner">
    <div class="loader"></div>
        <p>Uploading Word</p>
        </div>
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form id="form_data" name="form_data" enctype="multipart/form-data">
                    <input type="hidden" id="action" name="action" value="add">
                    <input type="hidden" id="id_kamus" name="id_kamus">
                    <div class="modal-header">
                        <h4 class="modal-title">Kosakata</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kosakata</label>
                            <input type="text" id="kosakata" name="kosakata" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Kosakata">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Video</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="video" name="video">
                                    <label class="custom-file-label" for="exampleInputFile" id="labelkosakata">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                        <label>Select Tema</label>
                            <select class="form-control" id="tema" name="tema">
                                <?php
                                foreach ($tema as $tema) { 
                                    echo "<option value='".$tema['id_tema']."'>".$tema['nama_tema']."</option>";
                                } ?>
                            </select>
                        </div>
                    </div>
                    </form>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" onclick="submit()" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
    </div>
    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog" >
          <div class="modal-content bg-danger">
            <div class="modal-header">
            <input type="hidden" id="kata" >
              <h4 class="modal-title">Hapus Kata</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Ingin menghapus kata ini ?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
              <button type="button" class="btn btn-outline-light" onclick ="hapuskata()">Ya</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
    </div> 
<script>
    function add() {
        $('#action').val('add');
        $('#id_kata').val('');
        $('#kosakata').val('');     
        $('#labelkosakata').html('Choose file');
    }
    
    function edit(id) {
        $('#action').val('edit');
        $.ajax({
            url : "<?= base_url('kamus/getkata') ?>",
            type : 'post',
            dataType : 'json',
            data : {
                id: id
            },
            success : function (data){
                $('#action').val('edit');
                $('#id_kamus').val(data[0].id_kamus);
                $('#kosakata').val(data[0].kata);
                $('#labelkosakata').html(data[0].video_kata);
                $('select[name="tema"]').val(data[0].tema);
            },
            error : function(){
            }
        })    
    }

    function submit(){
        $("div.spanner").addClass("show");
        $("div.overlay").addClass("show");
        var data = new FormData();
        //Form data
        var form_data = $('#form_data').serializeArray();
        $.each(form_data, function (key, input) {
            data.append(input.name, input.value);
        });

        //File data
        var file_data = $('input[name="video"]')[0].files;
        data.append("video", file_data[0]);
        // alert(data);
        // var data = $('#form_data').serialize();
        $.ajax({
            url : "<?= base_url('kamus/action') ?>",
            method: "post",
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
            $("div.spanner").removeClass("show");
            $("div.overlay").removeClass("show");
            location.reload();   
            console.log(data);
            },
            error: function (e) {
            console.log(JSON.stringify(e));
            $("div.spanner").removeClass("show");
            $("div.overlay").removeClass("show");             
            }
        })
    }

    function hapuskata(){
        var id = $('#kata').val();
        $.ajax({
            url : "<?= base_url('kamus/delete') ?>",
            method : 'post',
            data : {
                id: id
            },success : function(data){
                // alert(data)
                location.reload();
            }
        })
    }

    function hapus(id){
        $('#kata').val(id);
        
    } 

    var myVar;

    function myFunction() {
  myVar = setTimeout(showPage, 300);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}
</script>