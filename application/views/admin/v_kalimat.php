  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Kalimat</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Kalimat</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>    
    <section class="content">
        <div class="container-fluid">
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-lg" onclick="add()">Tambah Data</button>
        </div><br>
        <div class="container-fluid">
        <?php if($this->session->flashdata("message") == "success"){ ?>
        <div class="alert alert-success" role="alert">
          Success Tambah Kalimat.
        </div>
        <?php } ?>
        <?php if($this->session->flashdata("message") == "error"){ ?>
        <div class="alert alert-danger" role="alert">
          Gagal Tambah kalimat.
        </div>
        <?php } ?>
        <div class="card">
    <!-- /.card-header -->
          <div class="card-body">
          <table id="example1" class="table table-bordered table-striped">
              <thead>
              <tr>
                  <td>No</td>
                  <td>Kalimat</td>
                  <td>Video</td>
                  <td>Tema</td>
                  <td>Jumlah Kata</td>
                  <td>Aksi</td>
              </tr>
              </thead>
              <tbody>
              <?php
                      $i=1;
                          // var_dump($allmhs);
                          if(!empty($getkalimat)){
                              foreach ($getkalimat as $kl){
                                  echo ' 
                                  <tr>
                                  <td>'.$i.'</td>
                                  <td>'.$kl['kalimat'].'</td>
                                  <td>'.$kl['video_kalimat'].'</td>
                                  <td>'.$kl['tema'].'</td>
                                  <td>'.$kl['jumlah_kata'].'</td>
                                  <td>
                                      <button type="button" class="btn btn-info" onclick="edit('.$kl['id_kalimat'].')" data-toggle="modal" data-target="#modal-lg">Edit</button>
                                      <button type="button" class="btn btn-danger" onclick ="hapus('.$kl['id_kalimat'].')" data-toggle="modal" data-target="#modal-danger"><i class="fas fa-trash-alt"></i></button>
                                  </td>
                                  </tr>';
                                  $i+=1;
                              }
                          }else{
                              echo 
                              '<tr>
                              <td colspan="6" align="center">Tidak ada data</td>
                              </tr>';
                          }
                      ?>
              </tbody>
              <tfoot>
              <tr>
                  <td>No</td>
                  <td>Kalimat</td>
                  <td>Video</td>
                  <td>Tema</td>
                  <td>Jumlah Kata</td>
                  <td>Aksi</td>
              </tr>
              </tfoot>
          </table>
          </div>
          <!-- /.card-body -->
          </div>
      </div>
        </div>
    </section>
    <!-- modal tambah kata -->
    <div class="modal fade" id="modal-lg">
      <div class="overlay"></div>
        <div class="spanner">
        <div class="loader"></div>
        <p>Uploading Kalimat</p>
      </div>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="form_data" name="form_data" enctype="multipart/form-data">
                <input type="hidden" id="action" name="action" value="add">
                <input type="hidden" id="id_kalimat" name="id_kalimat">
                <div class="modal-header">
                    <h4 class="modal-title">Kalimat</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Kalimat</label>
                        <input type="text" id="kalimat" name="kalimat" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Kalimat">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Video</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="video" name="video">
                                <label class="custom-file-label" for="exampleInputFile" id="labelkalimat">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                            <label>Select Tema</label>
                            <select class="form-control" id="tema" name="tema">
                                <?php
                                foreach ($tema as $tema) { 
                                    echo "<option value='".$tema['id_tema']."'>".$tema['nama_tema']."</option>";
                                } ?>
                            </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Jumlah Kata</label>
                        <input type="text" id="jumlahkata" name="jumlahkata" class="form-control" id="exampleInputEmail1" placeholder="Masukkan Jumlah Kata">
                    </div>
                </div>
                </form>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" onclick="submit()" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-danger">
        <div class="modal-dialog" >
          <div class="modal-content bg-danger">
            <div class="modal-header">
            <input type="hidden" id="kalimat" >
              <h4 class="modal-title">Hapus Kalimat</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Ingin menghapus kalimat ini ?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
              <button type="button" class="btn btn-outline-light" onclick ="hapuskalimat()">Ya</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>

    </div>

<script>
    function add() {
        $('#action').val('add');
        $('#id_kaliamt').val('');
        $('#kalimat').val('');     
        $('#labelkalimat').html('Choose file');
        $('#jumlahkata').val(''); 
    }
    
    function edit(id) {
        $('#action').val('edit');
        $.ajax({
            url : "<?= base_url('kalimat/getkalimat') ?>",
            type : 'post',
            dataType : 'json',
            data : {
                id: id
            },
            success : function (data){
                $('#action').val('edit');
                $('#id_kalimat').val(data[0].id_kalimat);
                $('#kalimat').val(data[0].kalimat);
                $('#labelkalimat').html(data[0].video_kalimat);
                $('select[name="tema"]').val(data[0].tema);
                $('#jumlahkata').val(data[0].jumlah_kata);
            },
            error : function(){
            }
        })    
    }

    function submit(){
      $("div.spanner").addClass("show");
      $("div.overlay").addClass("show");
        var data = new FormData();
        //Form data
        var form_data = $('#form_data').serializeArray();
        $.each(form_data, function (key, input) {
            data.append(input.name, input.value);
        });
        //File data
        var file_data = $('input[name="video"]')[0].files;
        data.append("video", file_data[0]);
        // alert(data);
        // var data = $('#form_data').serialize();
        $.ajax({
            url : "<?= base_url('kalimat/action') ?>",
            method: "post",
            processData: false,
            contentType: false,
            data: data,
            success: function (data) {
              // console.log(data)
            $("div.spanner").removeClass("show");
            $("div.overlay").removeClass("show");
            location.reload(); 
            },
            error: function (e) {
            console.log(JSON.stringify(e));
            $("div.spanner").removeClass("show");
            $("div.overlay").removeClass("show");             
            }
        })
    }

    function hapuskalimat(){
        var id = $('#kalimat').val();
        $.ajax({
            url : "<?= base_url('kalimat/delete') ?>",
            method : 'post',
            data : {
                id: id
            },success : function(data){
                // alert(data)
                location.reload();
            }
        })
    }

    function hapus(id){
        $('#kalimat').val(id);
        
    } 

    var myVar;

    function myFunction() {
  myVar = setTimeout(showPage, 300);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

</script>