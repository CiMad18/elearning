<!-- Page Main -->
<div role="main" class="main" >
	<div class="page-default bg-white typo-dark" style="padding-top: 50px;">
	<!-- Container -->
		<div class="container event-sm-col">
        <h3 class="fonttittle">Buku Pelajaran</h3>
			<div class="row">
                <?php 
                foreach($getbuku as $gb){ 
                echo '
				<div class="col-sm-6 col-md-3">
					<div class="event-wrap">
						<div class="event-img-wrap">
						<img alt="Event" class="img-responsive" src="'.$cover_buku.$gb['foto_buku'].'" width="600" height="220">
						</div>
						<div class="event-details">
							<h5><a href="event-single-left.html">'.$gb['nama_buku'].'</a></h5>
                            <a href="bukuclient/download/'.$gb['file_buku'].'" class="btn">Unduh</a>
                            <a href="'.$file_buku.$gb['file_buku'].'" class="btn">Buka</a>
                        </div>
			            <hr class="lg hidden-991">
                    </div>
				</div>';
                    }
                ?><!-- Column -->
			</div><!-- Row -->
		</div><!-- Container -->
	</div><!-- Page Default -->
</div><!-- Page Main -->
