<style>
.filterDiv {
  text-align: center;
  display: none;
}

.show {
  display: block; 
}


.btn {
  border: none;
  outline: none;
  cursor: pointer;
}

.btn:hover {
  background-color: #ddd;
}

.btn.active {
  background-color: #666;
  color: white;
}
</style>
<!--==============================Content=================================--> 

<section id="content" style="padding-top: 50px;">
  <div class="container">	
		<div class="row clearfix">
	      	<div class="span12">
			<h3 class="fonttittle">Kamus</h3>
				<!-- Widget -->
				<div class="title-container sm text-left">
					<div class="col-md-12">
						<div class="widget no-box">
							<div class="search">
								<div class="input-group">
									<input type="text" class="form-control search " id="input_search" onkeyup="search()" placeholder="Cari kata..." 
									autocomplete="off" autofocus>
									<span class="input-group-btn">
										<button class="btn" id="btn_search" disabled><i class="fa fa-search"></i></button>
									</span>
								</div>
							</div>
						</div><!-- widget -->
					</div>
				</div>
				<div id="options" class="clearfix">
					<ul id="filters" class="pagination option-set clearfix" data-option-key="filter">
						<li><a class="btn kt active" onclick="filterSelection('all')">Show All</a></li>
						<li><a class="btn kt" onclick="filterSelection('A')">A</a></li>
						<li><a class="btn kt" onclick="filterSelection('B')">B</a></li>
						<li><a class="btn kt" onclick="filterSelection('C')">C</a></li>
						<li><a class="btn kt" onclick="filterSelection('D')">D</a></li>
						<li><a class="btn kt" onclick="filterSelection('E')">E</a></li>
						<li><a class="btn kt" onclick="filterSelection('F')">F</a></li>
						<li><a class="btn kt" onclick="filterSelection('G')">G</a></li>
						<li><a class="btn kt" onclick="filterSelection('H')">H</a></li>
						<li><a class="btn kt" onclick="filterSelection('I')">I</a></li>
						<li><a class="btn kt" onclick="filterSelection('J')">J</a></li>
						<li><a class="btn kt" onclick="filterSelection('K')">K</a></li>
						<li><a class="btn kt" onclick="filterSelection('L')">L</a></li> 
						<li><a class="btn kt" onclick="filterSelection('M')">M</a></li>
						<li><a class="btn kt" onclick="filterSelection('N')">N</a></li>
						<li><a class="btn kt" onclick="filterSelection('O')">O</a></li>
						<li><a class="btn kt" onclick="filterSelection('P')">P</a></li>
						<li><a class="btn kt" onclick="filterSelection('Q')">Q</a></li>
						<li><a class="btn kt" onclick="filterSelection('R')">R</a></li>
						<li><a class="btn kt" onclick="filterSelection('S')">S</a></li>
						<li><a class="btn kt" onclick="filterSelection('T')">T</a></li>
						<li><a class="btn kt" onclick="filterSelection('U')">U</a></li>
						<li><a class="btn kt" onclick="filterSelection('V')">V</a></li>
						<li><a class="btn kt" onclick="filterSelection('W')">W</a></li>
						<li><a class="btn kt" onclick="filterSelection('X')">X</a></li>
						<li><a class="btn kt" onclick="filterSelection('Y')">Y</a></li>
						<li><a class="btn kt" onclick="filterSelection('Z')">Z</a></li>
					</ul>
				</div>
					<ul class="thumbnails portfolio">
						<div class="row">
							<div class="col-md-12">
							<li id="maincontent">
							</li>
							</div>
						</div>
					</ul> 
					<nav>
					<ul class="pagination" id="pgContainer"">
						<li class="page-item">
						<span class="page-link" style="pointer-events: none;">Sebelumnya</span>
						</li>
						<li><a class="page-link btn pg pg1 active" onclick="paging(1)">1</a></li>
						<?php
						$i=2;
						do {
							echo '
							<li><a class="page-link btn pg" onclick="paging('.$i.')">'.$i.'</a></li>
							'; 
							$i+=1;
						  } while ($i <= $total_rows);
						?>
						<li class="page-item">
						<span class="page-link" <?php echo ($total_rows == 1) ? 'style="pointer-events: none;"' : ''; ?>>Selanjutnya</span>
						</li>
					</ul>
					</nav>
				</div>
		</div>  <!-- end of .row -->
  	</div> <!-- end of .container -->
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="titlepop"></h4>
		</div>
		<div class="modal-body">
			<div id="videoframe" class="embed-responsive embed-responsive-16by9">
				<video  width="100%" controls style="overflow: hidden; padding: 0; float: left;">
					<source id="videopop" src="" />
				</video>
			</div>
		</div>
    </div>
  </div>
</div>
<script>
var kategori = '';
var page = '';
$( document ).ready(function() {
    page=1;
	kategori='all';
	getdata(kategori,page);
	setBtn();
});

function filterSelection(c) {
	kategori = c;
	page = 1;
	getpage(c);
	getdata(kategori, page);
	$('.pg').removeClass('active');
	$('.pg1').addClass('active');
}

function paging(c) {
	page = c;
	getdata(kategori,page);
	setBtn();
}

var btnContainer = document.getElementById("options");
var btns = btnContainer.getElementsByTagName("a");
for (var i = 0; i < btns.length; i++) {
btns[i].addEventListener("click", function(){
	var current = document.getElementsByClassName("active");
	$('.kt').removeClass('active');
	$(this).addClass('active');
});
}

function setBtn() {

	var pgContainer = document.getElementById("pgContainer");
	var btnPg = pgContainer.getElementsByTagName("a");
	for (var i = 0; i < btnPg.length; i++) {
		btnPg[i].addEventListener("click", function(){
		var current = document.getElementsByClassName("active");
		$('.pg').removeClass('active');
		$(this).addClass('active');
	});
	}
}
	
function getdata(kt, pg) {
	$.ajax({
		url : "<?= base_url('Kamusclient/getData') ?>",
		method: "post",
		data: {
			kategori : kt,
			page : pg
		},
		success: function (data) {
			data = JSON.parse(data);
			var html = '';
			for (var i=0; i<data.length; i++) {
				var url = '<?= $thumbnail_url ?>'+(data[i].video_kata).substr(0, (data[i].video_kata).lastIndexOf('.'))+".jpeg";
				var click = "getvideo('"+data[i].id_kamus+"','"+data[i].kata+"','"+data[i].video_kata+"')";
				html+= '<div class="col-md-4">';
				html+= '<div class="thumbnail clearfix inibox">';
				html+= '<div class="imgarea" style="text-align:center">';
				html+= '<img width="100%" controls style="overflow: hidden; padding: 0; float: left;" data-toggle="modal" onclick="'+click+'")" data-target="#myModal" src="'+url+'" alt="">';
				html+= '<button type="button" style="margin-top:4px" class="btn btn-primary btn-lg" data-toggle="modal" onclick="'+click+'" data-target="#myModal">';
				html+= data[i].kata;
				html+= '</button></div></div></div>';
			} 
			$('#maincontent').html(html);
		},
		error: function (e) {
		console.log(JSON.stringify(e));
		}
	})
}

function getpage(kt) {
	$.ajax({
		url : "<?= base_url('Kamusclient/getpage') ?>",
		method: "post",
		data: {
			kategori : kt,
		},
		success: function (data) {
			var html = '';
			html += '<li class="page-item"><span class="page-link" style="pointer-events: none;">Sebelumnya</span></li>';
			html += '<li><a class="page-link btn pg pg1 active" onclick="paging(1)">1</a></li>';
			for (var i=2; i<=data; i++) {
				html += '<li><a class="page-link btn pg" onclick="paging('+i+')">'+i+'</a></li>';
			}
			if(data == 1)html += '<li class="page-item"><span class="page-link" style="pointer-events: none;">Selanjutnya</span></li>';
			else html += '<li class="page-item"><span class="page-link">Selanjutnya</span></li>'
			$('#pgContainer').html(html);
			setBtn();
		},
		error: function (e) {
		console.log(JSON.stringify(e));
		}
	})
}

function getvideo(id, kata, video) {
	
	$('#videopop').removeAttr("src").attr("src", '<?= $kamus_url?>'+video);
	$('#titlepop').html(kata);
   	document.getElementById("videoframe").innerHTML = document.getElementById("videoframe").innerHTML;
}

function search() {
	var key_search = $('#input_search').val();
	$.ajax({
		url : "<?= base_url('Kamusclient/getSearch') ?>",
		method: "post",
		data: {
			key : key_search
		},
		success: function (data) {
			data = JSON.parse(data);
			var html = '';
			for (var i=0; i<data.length; i++) {
				var url = '<?= $thumbnail_url ?>'+(data[i].video_kata).substr(0, (data[i].video_kata).lastIndexOf('.'))+".jpeg";
				var click = "getvideo('"+data[i].id_kamus+"','"+data[i].kata+"','"+data[i].video_kata+"')";
				html+= '<div class="col-md-4">';
				html+= '<div class="thumbnail clearfix inibox">';
				html+= '<div class="imgarea" style="text-align:center">';
				html+= '<img width="100%" controls style="overflow: hidden; padding: 0; float: left;" data-toggle="modal" onclick="'+click+'")" data-target="#myModal" src="'+url+'" alt="">';
				html+= '<button type="button" style="margin-top:4px" class="btn btn-primary btn-lg" data-toggle="modal" onclick="'+click+'" data-target="#myModal">';
				html+= data[i].kata;
				html+= '</button></div></div></div>';
			} 
			$('#maincontent').html(html);
		},
		error: function (e) {
		console.log(JSON.stringify(e));
		}
	})
}

</script>