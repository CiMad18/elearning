<!DOCTYPE html>
<html>
	<head>
		<!-- Basic -->
		<meta charset="utf-8">
		<title>Elearning Deaf Student</title>
		<meta name="keywords" content="Universh - Material Education, Events, News, Learning Centre & Kid School MultiPurpose HTML5 Template" />
		<meta name="description" content="Universh - Material Education, Events, News, Learning Centre & Kid School MultiPurpose HTML5 Template">
		<meta name="author" content="glorythemes.in">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/klient2/images/default/logotittle.png">
		<!-- Web Fonts  -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' 
		rel='stylesheet' type='text/css'>

		<!-- Lib CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/univershicon.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/prettyPhoto.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/menu.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/timeline.css">
		
		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme-responsive.css">
		
		<!--[if IE]>
			<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/ie.css">
		<![endif]-->
		
		<!-- Skins CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/skins/default.css">
		
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/custom.css">
	</head>
<body>	
<!-- Page Main -->
<div class="main relative bg-img bg-cover overlay bg-color heavy" data-background="<?php echo base_url();?>assets/klient2/images/galeri/login-bg.jpg"  data-stellar-background-ratio="0.5">
	<!-- Section -->
	<div class="page-default typo-dark">
		<!-- Container -->
		<div class="container">
			<!-- Row -->
			<div class="row">
				<div class="col-md-offset-3 col-md-6 parent-has-overlay">
					<ul class="template-box box-login">
						<!-- Page Template Logo -->
						<li class="logo-wrap  text-center">
							<a href="index.html" class="logo">
								<img width="300px" height="100px" src="<?php echo base_url();?>assets/klient2/images/default/fixsekalilogo.png">
								<p class="slogan">Buat Akun</p>
							</a>
						</li><!-- Page Template Logo -->
						<!-- Page Template Content -->
						<li class="template-content text-left">
							<div class="contact-form" id="form_data" name="form_data" >
                            <input type="hidden" id="action" name="action" value="add">
                            <input type="hidden" id="id_pengguna" name="id_pengguna">
								<!-- Form Begins -->
								<form name="bootstrap-form" action="<?php echo base_url(); ?>register/aksi_register" method="POST">
									<!-- Field 1 -->
									<div class="input-text form-group">
										<label>Nama Pengguna</label>
										<input id="pengguna" name="pengguna" type="text" class="input-name form-control" />
									</div>
									<!-- Field 1 -->
									<div class="input-text form-group">
										<label>Nama Lengkap</label>
										<input id="nama_lengkap" name="nama_lengkap" type="text" class="input-name form-control" />
                                    </div>
                                    <div class="input-text form-group">
										<label>No Handphone</label>
										<input id="no_hp" name="no_hp" type="text" class="input-name form-control" />
                                    </div>
									<!-- Field 1 -->
									<div class="input-text form-group">
										<label>Password</label>
										<input id="password1" name="password1" type="password" value="" class="form-control" />
									</div>
									<!-- Field 2 -->
									<div class="form-group">
										<label>Re-enter Password</label>
										<input id="password2" name="password2" type="password" value="" class="form-control" />
										<p id="validate-status"></p>
                                    </div>
									<!-- Button -->
									<button class="btn btn-block" type="submit" value="Register">Create my Account</button>
								</form><!-- Form Ends -->
							</div>	
						</li><!-- Page Template Content -->
					</ul>
				</div><!-- Column -->
			</div><!-- Row -->
		</div><!-- Container -->	
	</div><!-- Page Default -->
</div><!-- Page Main -->


<!-- library -->
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.appear.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.easing.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/countdown.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/counter.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.mb.YTPlayer.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.prettyPhoto.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.stellar.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/menu.js"></script>

<script src="<?php echo base_url();?>assets/klient2/js/lib/modernizr.js"></script>
<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url();?>assets/klient2/js/theme.js"></script>

<!-- Theme Custom -->
<script src="<?php echo base_url();?>assets/klient2/js/custom.js"></script>

</body>
</html>
<script>
   $(document).ready(function() {
  $("#password2").keyup(validate);
});

function validate() {
  var password1 = $("#password1").val();
  var password2 = $("#password2").val();
  console.log(password1);
 
    if(password1 == password2) {
       $("#validate-status").text("valid");        
    }
    else {
        $("#validate-status").text("invalid");  
    }
    
}
</script>