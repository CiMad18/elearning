<!DOCTYPE html>
<html>
	<head>
		<!-- Basic -->
		<meta charset="utf-8">
		<title>E-Learning Deaf Student</title>
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/klient2/images/default/logotittle.png">
		<!-- Web Fonts  -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' 
		rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'>

		<!-- Lib CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/univershicon.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/prettyPhoto.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/menu.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/timeline.css">
		
		<!-- Revolution CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/settings.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/layers.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/navigation.css"> 
		
		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme-responsive.css">
		
		<!--[if IE]>
			<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/ie.css">
		<![endif]-->
		
		<!-- Skins CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/skins/default.css">
		
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/custom.css">
	</head>
<body class="kids-font">

<!-- Page Loader -->
<div id="pageloader">
	<div class="loader-inner">
		<img src="<?php echo base_url();?>assets/klient2/images/default/preloader-color.gif" alt="">
	</div>
</div><!-- Page Loader -->

<!-- Back to top -->
<a href="#0" class="cd-top">Top</a>

<!-- End Theme Panel Switcher -->	
<!-- Header Begins -->	
<header id="header" class="default-header colored flat-menu" style="padding-bottom: 35px;">
	<div class="header-top">
	<div class="container">	
			<nav>
				<ul class="nav nav-pills nav-top">
					
				</ul>
			</nav>
		</div>
	</div>
	<div class="container">
		<div class="logo">
				 <img src="<?php echo base_url();?>assets/klient2/images/default/fixsekalilogo.png"></a></div>
				 <button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
				<i class="fa fa-bars"></i></button>
	</div>
	<div class="navbar-collapse nav-main-collapse collapse">
		<div class="container">
		<a class="col" href="<?php echo base_url();?>Register"><button type="button" class="btn btn-primary"  style="float: right; ">Buat Akun</button></a>
		<a class="col" href="<?php echo base_url();?>Login"><button type="button" class="btn btn-primary"  style="float: right; ">Masuk</button></a>
		</div>
	</div>
</header><!-- Header Ends -->