
<!-- Page Main -->
<div role="main" class="main">
<!-- Section -->
	<section  style="padding-top: 50px;">
		<div class="container">
		<h3 class="fonttittle">Tematik</h3>
			<div class="row">
			<?php
      		foreach($gettema as $gt){ 
			  echo '                    
			  <div class="col-sm-4" style="padding-bottom:5px">
				<!-- Event Wrapper -->
				<div class="event-wrap">
					<div class="event-img-wrap">
						<img alt="Event" src="'.$gambar_tema.$gt['gambar_tema'].'" style="object-fit: cover;width: 100%;max-height: 100%;">
					</div><!-- Event Image Wrapper -->
					<!-- Event Detail Wrapper --> 
					<div style="text-overflow: ellipsis;overflow:hidden;padding: 10px;position: relative;box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.14);">
						<h4 style="height:75px; text-align:left; text-size: 100px; "><a href="event-single-left.html">'.$gt['nama_tema'].'</a></h4>
						<a class="col" href="'.base_url().'Tematikclient/openpertema/'.$gt['id_tema'].'""><button type="button" class="btn btn-primary"  style="float: right; margin-top:5px ">Buka</button></a>
					</div><!-- Event Meta -->
				</div><!-- Event details -->
			</div><!-- Column -->
				';
			}
			?>
			</div>
		</div><!-- Container -->
	</section><!-- Section -->
</div><!-- Page Main -->
