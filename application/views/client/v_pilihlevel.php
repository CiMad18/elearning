<!DOCTYPE html>
<html>
	<head>
		<!-- Basic -->
		<meta charset="utf-8">
		<title>E-Learning Deaf Student</title>
		<meta name="keywords" content="Universh - Material Education, Events, News, Learning Centre & Kid School MultiPurpose HTML5 Template" />
		<meta name="description" content="Universh - Material Education, Events, News, Learning Centre & Kid School MultiPurpose HTML5 Template">
		<meta name="author" content="glorythemes.in">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Favicon -->
        <link rel="shortcut icon" href="images/default/logotittle.png">
		<!-- Web Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' 
		rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'>

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/custom.css">
		<!-- Lib CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/univershicon.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/prettyPhoto.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/menu.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/timeline.css">
		
		<!-- Revolution CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/settings.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/layers.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/navigation.css"> 
		
		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme-responsive.css">
		
		<!-- [if IE]> -->
			<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/ie.css">
		<!-- <![endif] -->
		
		<!-- Skins CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/skins/default.css">
		
		
	</head>
	<body>
		<div class="container">
			<div class="row" style="padding-top: 20px;">
				<div class="span12" style="padding-bottom: 150px;">
					<div role="main" class="main">
					<h1 class="fonttittle">Games</h1>
						<div class="bg-white typo-dark" style="padding-bottom: 100px;">
							<!-- Container -->
							<div class="container">
								<div class="row">
									<h3 style="text-align: center; margin-top:25px;">Pilih Level</h3>
									<div class="row">
										<div class="col-md-4">
											<a <?php echo "href=".base_url()."GamesClient/games/".$tema."/1" ?>><img  style="margin-left: auto;margin-right:auto;" width="100%" height="250px" src="<?php echo base_url();?>assets/klient2/images/level/1.png"  ></a>
										</div>
										<div class="col-md-4">
											<a <?php echo ($level > 1)? "href=".base_url()."GamesClient/games/".$tema."/2" : '' ?>><img  style="margin-left: auto;margin-right:auto;" width="100%" height="250px" src="<?php echo base_url();?>assets/klient2/images/level/<?php echo ($level > 1)? '2.png': 'lock2.png' ?>" ></a>
										</div>
										<div class="col-md-4">
											<a <?php echo ($level > 2)? "href=".base_url()."GamesClient/games/".$tema."/3" : '' ?>><img  style="margin-left: auto;margin-right:auto;" width="100%" height="250px" src="<?php echo base_url();?>assets/klient2/images/level/<?php echo ($level > 2)? '3.png': 'lock3.png' ?>"></a>
										</div>
										</div>
								</div><!-- Row -->
							</div><!-- Container -->
						</div><!-- Page Default -->
					</div><!-- Page Default -->
				</div>
			</div><!-- Page Main -->
		</div><!-- bg top  -->
	</body>
	</html>

<script src="<?php echo base_url();?>assets/klient/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.appear.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.easing.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/countdown.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/counter.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.mb.YTPlayer.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.prettyPhoto.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.stellar.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/klient2/js/lib/menu.js"></script> -->
<!-- Revolution Js -->
<script src="<?php echo base_url();?>assets/klient2/revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
<script src="<?php echo base_url();?>assets/klient2/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/theme-rs.js"></script>

<script src="<?php echo base_url();?>assets/klient2/js/lib/modernizr.js"></script>
<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url();?>assets/klient2/js/theme.js"></script>

</script>