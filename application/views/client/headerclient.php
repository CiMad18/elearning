<!DOCTYPE html>
<html>
	<head>
		<!-- Basic -->
		<meta charset="utf-8">
		<title>E-Learning Deaf Student</title>
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/klient2/images/default/logotittle.png">
		<!-- Web Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' 
		rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'>

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/custom.css">
		<!-- Lib CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/univershicon.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/prettyPhoto.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/menu.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/timeline.css">
		
		<!-- Revolution CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/settings.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/layers.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/navigation.css"> 
		
		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme-responsive.css">
		
		<!-- [if IE]> -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/ie.css">
		
		<!-- <![endif] -->
		
		<!-- Skins CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/skins/default.css">
		
		<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.js"></script>
		<script src="<?php echo base_url();?>assets/klient2/js/lib/bootstrap.min.js"></script>

		
	</head>
	<header id="header" class="default-header colored flat-menu">
	<div class="header-top">
		<div class="container">
			<nav>
				<!-- untuk foto profile -->

			</nav>
		</div>
	</div>
	<div class="container">
		<div class="logo">
				 <img width="250" height="50" src="<?php echo base_url();?>assets/klient2/images/default/fixsekalilogo.png"></a></div>
				 <button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
			<i class="fa fa-bars"></i></button>
	</div>
	<div class="navbar-collapse nav-main-collapse collapse">
		<div class="container">
			<nav class="nav-main mega-menu">
				<ul class="nav nav-pills nav-main" id="mainMenu">
					<li class="nav-item <?php if($title == "Buku Pelajaran")echo "active"?>">
						<a href="<?php echo base_url();?>Bukuclient">Buku Pelajaran </a>
					</li>
					<li class="nav-item <?php if($title == "Kamus")echo "active"?>">
						<a  href="<?php echo base_url();?>Kamusclient" >Kamus</a>
					</li>
					<li class="nav-item <?php if($title == "Tematik")echo "active"?>">
						<a href="<?php echo base_url();?>Tematikclient">Tematik</a>
					</li>
					<li class="nav-item <?php if($title == "Games")echo "active"?>">
						<a href="<?php echo base_url();?>Gamesclient">Games</a>
					</li>
					<li class="dropdown mega-menu-item mega-menu-signin signin logged" id="headerAccount">
						<a class="dropdown-toggle" href="#">
							<i class="fa fa-user"><?= $_SESSION['namapengguna']?></i>
							<i class="fa fa-caret-down"></i>
						</a>
						<ul class="dropdown-menu">
							<li>
								<div class="mega-menu-content">
									<div class="row">
										<div class="col-md-8">
											<div class="user-avatar">
												<div class="img-thumbnail">
													<img src="<?= base_url().'assets/fotoprofile/'.$_SESSION['fotoprofile']?>" alt="" />
												</div>
												<p><strong><?= $_SESSION['namapengguna']?></strong><span><?= $_SESSION['rolele']?></span></p>
												<a href="<?php echo base_url();?>Logout"><button class="btn">Logout</button></a>
											</div>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</header><!-- Header Ends -->
<body class="kids-font">

<!-- Page Loader -->
<div id="pageloader">
	<div class="loader-inner">
		<img src="<?php echo base_url();?>assets/klient2/images/default/preloader-color.gif" alt="">
	</div>
</div><!-- Page Loader -->
<!-- Header Begins -->	
