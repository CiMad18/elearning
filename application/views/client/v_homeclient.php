<!-- Page Main -->
<div role="main" class="main">
	<!-- Kids Slider -->
	<div class="rs-container rev_slider_wrapper">
		<div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"delay": 9000, "gridwidth": 1170, "gridheight": 600}'>
			<ul>
				<li data-transition="fade" class="typo-dark heavy">
					<div class="tp-caption sm-text"
						data-x="left" data-hoffset="30"
						data-y="center" data-voffset="-65"
						data-start="500"
						data-transform_in="y:[-300%];opacity:0;s:500;">
						Education For Everyone
					</div>
					<div class="tp-caption big-text"
						data-x="left" data-hoffset="30"
						data-y="center" data-voffset="-5"
						data-start="1500"
						data-whitespace="nowrap"						 
						data-transform_in="y:[100%];s:500;"
						data-transform_out="opacity:0;s:500;"
						data-mask_in="x:0px;y:0px;">
							<span class="color-orange">Dream </span> 
							<span class="color-yellow">& </span> 
							<span class="color-voilet">Create</span> 
					</div>
					<div class="tp-caption sm-text"
						data-x="left" data-hoffset="30"
						data-y="center" data-voffset="60"
						data-start="2000"
						data-transform_in="y:[100%];opacity:0;s:500;">Opening up a world class of education</div>
						
						<div class="tp-caption" 
						data-x="630" 
						data-y="bottom" data-voffset="-20" 
						data-start="2100" 
						data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:500;e:Power2.easeOut;">
							<img src="<?php echo base_url();?>assets/klient2/images/kids/lp2.jpg" alt="">
					</div>
				</li>
			</ul>
		</div>
	</div><!-- Kids Slider -->
	
	<!-- Section -->
	<section class="bg-lgrey typo-dark">
		<div class="container">
			<div class="row counter-sm">
				<!-- Title -->
				<div class="col-sm-12">
					<div class="title-container sm text-left">
						<div class="title-wrap">
							<h4 class="title">About E-Learning</h4>
							<span class="separator line-separator"></span>
						</div>
					</div> 
				</div>
				<!-- Title -->
				<div class="col-sm-6 col-md-4">
					<!-- Count Block -->
					<div class="count-block dark bg-green">
						<h5>Buku Pelajaran</h5>
						<h3 data-count="<?= $x; ?>" class="count-number"><span class="counter"></span></h3>
						<i class="uni-box-withfolders"></i>
					</div><!-- Counter Block -->
				</div><!-- Column -->
				<div class="col-sm-6 col-md-4">
					<!-- Count Block -->
					<div class="count-block dark bg-yellow">
						<h5>Kosakata</h5>
						<h3 data-count="<?= $z; ?>" class="count-number"><span class="counter"></span></h3>
						<i class="uni-normal-text"></i>
					</div><!-- Counter Block -->
				</div><!-- Column -->
				<div class="col-sm-6 col-md-4">
					<!-- Count Block -->
					<div class="count-block dark bg-pink">
						<h5>Pengguna</h5>
						<h3 data-count="<?= $y; ?>" class="count-number"><span class="counter"></span></h3>
						<i class="uni-talk-man"></i>
					</div><!-- Counter Block -->
				</div><!-- Column -->
			</div><!-- Row -->
		</div><!-- Container -->
	</section><!-- Section -->
<div role="main" class="main">
	<div class="page-default bg-white typo-dark">
		<!-- Container -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3> Galeri Sekolah</h3>
					<!-- Gallery Block -->
					<div class="isotope-grid grid-three-column" data-gutter="20" data-columns="3">
						<div class="grid-sizer"></div>
						<!-- Portfolio Item -->
						<div class="item all design web">
							<!-- Image Wrapper -->
							<div class="image-wrapper">
								<!-- IMAGE -->
								<img src="<?php echo base_url();?>assets/klient2/images/galeri/1.jpg" alt="" />
								<!-- Gallery Btns Wrapper -->
								<div class="gallery-detail btns-wrapper">
									<a href="gallery-single.html" class="btn uni-upload-2"></a>
									<a href="images/gallery/grid/1.jpg" data-rel="prettyPhoto[portfolio]" class="btn uni-full-screen"></a>
								</div><!-- Gallery Btns Wrapper -->
							</div><!-- Image Wrapper -->
						</div><!-- Portfolio Item -->
						<!-- Portfolio Item -->
						<div class="item all design identity photography">
							<!-- Image Wrapper -->
							<div class="image-wrapper">
								<!-- IMAGE -->
								<img src="<?php echo base_url();?>assets/klient2/images/galeri/2.jpg" alt="" />
								<!-- Gallery Btns Wrapper -->
								<div class="gallery-detail btns-wrapper">
									<a href="gallery-single.html" class="btn uni-upload-2"></a>
									<a href="images/gallery/grid/2.jpg" data-rel="prettyPhoto[portfolio]" class="btn uni-full-screen"></a>
								</div><!-- Gallery Btns Wrapper -->
							</div><!-- Image Wrapper -->
						</div><!-- Portfolio Item -->
						<!-- Portfolio Item -->
						<div class="item all identity web">
							<!-- Image Wrapper -->
							<div class="image-wrapper">
								<!-- IMAGE -->
								<img src="<?php echo base_url();?>assets/klient2/images/galeri/3.jpg" alt="" />
								<!-- Gallery Btns Wrapper -->
								<div class="gallery-detail btns-wrapper">
									<a href="gallery-single.html" class="btn uni-upload-2"></a>
									<a href="images/gallery/grid/3.jpg" data-rel="prettyPhoto[portfolio]" class="btn uni-full-screen"></a>
								</div><!-- Gallery Btns Wrapper -->
							</div><!-- Image Wrapper -->		 
						</div><!-- Portfolio Item -->
						<!-- Portfolio Item -->
						<div class="item all design web">
							<!-- Image Wrapper -->
							<div class="image-wrapper">
								<!-- IMAGE -->
								<img src="<?php echo base_url();?>assets/klient2/images/galeri/4.jpg" alt="" />
								<!-- Gallery Btns Wrapper -->
								<div class="gallery-detail btns-wrapper">
									<a href="gallery-single.html" class="btn uni-upload-2"></a>
									<a href="images/gallery/grid/4.jpg" data-rel="prettyPhoto[portfolio]" class="btn uni-full-screen"></a>
								</div><!-- Gallery Btns Wrapper -->
							</div><!-- Image Wrapper -->		 
						</div><!-- Portfolio item -->
						<!-- Portfolio Item -->
						<div class="item all identity">
							<!-- Image Wrapper -->
							<div class="image-wrapper">
								<!-- IMAGE -->
								<img src="<?php echo base_url();?>assets/klient2/images/galeri/5.jpg" alt="" />
								<!-- Gallery Btns Wrapper -->
								<div class="gallery-detail btns-wrapper">
									<a href="gallery-single.html" class="btn uni-upload-2"></a>
									<a href="images/gallery/grid/5.jpg" data-rel="prettyPhoto[portfolio]" class="btn uni-full-screen"></a>
								</div><!-- Gallery Btns Wrapper -->
							</div><!-- Image Wrapper -->		 
						</div><!-- Portfolio item -->
						<!-- Portfolio Item -->
						<div class="item all identity web">
							<!-- Image Wrapper -->
							<div class="image-wrapper">
								<!-- IMAGE -->
								<img src="<?php echo base_url();?>assets/klient2/images/galeri/6.jpg" alt="" />
								<!-- Gallery Btns Wrapper -->
								<div class="gallery-detail btns-wrapper">
									<a href="gallery-single.html" class="btn uni-upload-2"></a>
									<a href="images/gallery/grid/6.jpg" data-rel="prettyPhoto[portfolio]" class="btn uni-full-screen"></a>
								</div><!-- Gallery Btns Wrapper -->
							</div><!-- Image Wrapper -->		 
						</div><!-- Portfolio item -->
					</div><!-- Gallery Block -->
				</div><!-- Column -->
			</div><!-- Row -->
		</div><!-- Container -->
	</div><!-- Page Default -->
</div><!-- Page Main --