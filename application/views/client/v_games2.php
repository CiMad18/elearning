<!DOCTYPE html>
<html>
	<head>
		<!-- Basic -->
		<meta charset="utf-8">
		<title>E-Learning Deaf Student</title>
		<meta name="keywords" content="Universh - Material Education, Events, News, Learning Centre & Kid School MultiPurpose HTML5 Template" />
		<meta name="description" content="Universh - Material Education, Events, News, Learning Centre & Kid School MultiPurpose HTML5 Template">
		<meta name="author" content="glorythemes.in">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url();?>assets/klient2/images/default/logotittle.png">
		<!-- Web Fonts -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' 
		rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'>

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/custom.css">
		<!-- Lib CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/univershicon.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/prettyPhoto.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/menu.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/timeline.css">
		
		<!-- Revolution CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/settings.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/layers.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/revolution/css/navigation.css"> 
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Sofia">
		
		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme-responsive.css">
		
		<!-- [if IE]> -->
			<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/ie.css">
			<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/fonts/BubblegumSans-Regular.ttf">
		<!-- <![endif] -->
	
		<!-- Skins CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/skins/default.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/skins/halamangame.css">
	</head>
	<body>
	<div class="itemquiz">
		<div id="games" class="games">
			<div class="row">
			  	<h3 style="font-family: Sofia, sans-serif; color:black;">Tebak Kosakata Isyarat</h3>
				<!-- Container -->
				<h2 id="counter" class="texttt"></h2>
				<div class="row col-md-12 containerQuizz" >
					<!-- List row -->
					<div class="quizz">
						<div class="col-md-8" style="padding-bottom: 5%; padding-top: 2%;">
							<div class="imgarea" id="videoarea">
								<video width="70%" controls style="display:block;margin-left:auto;margin-right:auto;overflow: hidden; padding: 0; float: center;">
									<source id="video" src=""/>
								</video>
							</div>  
						</div>
						<div class="col-md-4" style="padding-bottom: 5%;padding-top: 2%;">
							<div class="col-md-12" style="margin-bottom:10%;">
							<input type="button" class="block" value="" onclick="" id="A1">
							</div>
							<div class="col-md-12" style="margin-bottom:10%;">
							<input type="button" class="block" value="" onclick="" id="A2">
							</div>
							<div class="col-md-12" style="margin-bottom:10%;">
							<input type="button" class="block" value="" onclick="" id="A3">
							</div>
							<div class="col-md-12" style="margin-bottom:10%;">
							<input type="button" class="block" value="" onclick="" id="A4">
							</div>
						</div>
					</div>
				</div><!-- Row -->
			</div><!-- Page Main -->
		</div><!-- bg top  -->
		
		
		<div id="div_score" class="score" style="display:none">
		  <h1 style="font-family:BubblegumSans-Regular;">Score Kamu <span id="score"></span></h1>
		</div>
	</div>
	<div class="modal" id="correction">
      <div class="modal-dialog">
        <img id="img_correction" class="correction" style="width: 350px; height:550px">
      </div>
 	</div>
	</body>
	</html>

<script src="<?php echo base_url();?>assets/klient/js/bootstrap.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.appear.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.easing.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/countdown.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/counter.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.mb.YTPlayer.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.prettyPhoto.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.stellar.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/klient2/js/lib/menu.js"></script> -->

<!-- Revolution Js -->
<script src="<?php echo base_url();?>assets/klient2/revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
<script src="<?php echo base_url();?>assets/klient2/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/theme-rs.js"></script>

<script src="<?php echo base_url();?>assets/klient2/js/lib/modernizr.js"></script>
<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url();?>assets/klient2/js/theme.js"></script>

<script>
	<?php
		if($level == '1'){
			echo 'var base_url = "'.base_url().'assets/kamus/";';
		}else {
			echo 'var base_url = "'.base_url().'assets/kalimat/";';
		}
	?>
var pertanyaan = <?= json_encode($pertanyaan) ?>;
var video = <?= json_encode($video) ?>;
var jawaban = <?= json_encode($jawaban) ?>;
var img_correction_path = '<?= base_url() ?>assets/klient2/images/level/';
var c = 0;
var p = 1;
var score = 0;
$( document ).ready(function() {
	setpertanyaan();
	// $('#correction').modal({backdrop: 'static', keyboard: false})  
});
function set(jawab) {
	if(jawab == pertanyaan[c]){
		// alert('benar');
		console.log('benar');
		score = score + 10;
		$('#img_correction').attr('src', img_correction_path+'betul.png');
		next();
	}else{
		// alert('salah');
		console.log('salah');
		$('#img_correction').attr('src', img_correction_path+'salah.png');
		next();
	}
}
function next() {
	$('#correction').modal({backdrop: 'static', keyboard: false})  
	c++;
	p++;
	setTimeout(function() {
		if(c > 9){
			$('#correction').modal('hide') 	
			document.getElementById("games").style.display = "none";
			$('#score').html(score);
			document.getElementById("div_score").style.display = "block";
			// alert('selesai, score anda adalah '+score);
			var id_pengguna = '<?= $_SESSION['userlog'] ?>';
			var maxscore = '<?= $maxscore ?>';
			var maxlvl = '<?= $maxlvl ?>';
			var level = '<?= $level ?>';
			if (score > maxscore) {
				if(level >= maxlvl){
					$.ajax({
						url : "<?= base_url('Gamesclient/updatelevel') ?>",
						method: "post",
						data: {
							id_pengguna : id_pengguna,
							id_tema : '<?= $temaid ?>',
							maxlvl : maxlvl,
							level : level,
							score : score
						},
						success: function (data) {	
							// setTimeout(function() {
							// 	window.location.replace("<?= base_url().'Gamesclient/Pilihlevel/'.$tema?>");
							// }, 4000);
							console.log('berhasil update data')
						},
						error: function (e) {
							console.log(JSON.stringify(e));
						}
					})
				}
			}
			setTimeout(function() {
				window.location.replace("<?= base_url().'Gamesclient/Pilihlevel/'.$tema?>");
			}, 4000);
		}else{
			setpertanyaan();
			$('#correction').modal('hide') 
		} 
	}, 2000);
	
}
function setpertanyaan() {
	$('#counter').html('Level '+<?= $level?>+' Pertanyaan '+p);
//    console.log("<?= base_url()."assets/kamus/"?>"+video[0]);
   	$('#video').removeAttr("src").attr("src", base_url+video[c]);
	$('#A1').val(jawaban[c][0]).attr("onclick","set('"+jawaban[c][0]+"')");
	$('#A2').val(jawaban[c][1]).attr("onclick","set('"+jawaban[c][1]+"')");
	$('#A3').val(jawaban[c][2]).attr("onclick","set('"+jawaban[c][2]+"')");
	$('#A4').val(jawaban[c][3]).attr("onclick","set('"+jawaban[c][3]+"')");
   	document.getElementById("videoarea").innerHTML = document.getElementById("videoarea").innerHTML;
}
</script>