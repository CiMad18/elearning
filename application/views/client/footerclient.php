</body>
<!-- Footer -->
<footer id="footer" class="footer-7 relative">
	<div class="row-sep cloud"></div>
	<div class="main-footer widgets-dark typo-light">
		<div class="container">
			<div class="row">
				<!-- Widget Column -->
				<div class="col-md-3">
					<!-- Widget -->
					<div class="widget contact-widget no-box">
						<h5 class="widget-title">SLB Laniang Makassar</h5>
						<h6>Hubungi kami</h6>
							<ul>
								<li><a href="#"><i class="fa fa-map-marker"></i>0411  </a></li>
								<li><a href="#"><i class="fa fa-envelope"></i>slblaniangelearning.com</a></li>
							</ul>
					</div><!-- Widget -->
				</div><!-- Column -->
				<!-- Widget Column -->
				<div class="col-md-9">
					<!-- Widget -->
					<div class="widget subscribe no-box">
						<h5>Aplikasi E-Learning for Deaf Students <span></span></h5>
						<p class="form-message1" style="display: none;"></p>
						<div class="clearfix"></div>
						<form class="input-group subscribe-form" name="subscribe-form" method="post" action="php/subscription.php" id="subscribe-form">
							<div class="form-group has-feedback">
								<input class="form-control" type="email" placeholder="Subscribe" value="" name="subscribe_email">
							</div>
							<span class="input-group-btn">
								<a class="btn" type="submit"><span class="glyphicon glyphicon-arrow-right"></span></a>
							</span>
						</form>
					</div><!-- Widget -->
				</div>
				<!-- Column -->
				
			</div><!-- Row -->
		</div><!-- Container -->		
	</div><!-- Main Footer -->
	
	<!-- Footer Copyright -->
	<div class="footer-copyright">
		<div class="container">
			<div class="row">
				<!-- Copy Right Logo -->
				<div class="col-md-2">
					<a class="logo" href="index.html">
						<!-- <img src="images/default/logo-footer.png" width="211" height="40"  class="img-responsive" alt="Universh Education HTML5 Website Template"> -->
					</a>
				</div><!-- Copy Right Logo -->
				<!-- Copy Right Content -->
				<div class="col-md-6">
					<p>&copy; Copyright 2021. A.Cici Nurmadinah</a></p>
				</div><!-- Copy Right Content -->
				<!-- Copy Right Content -->
			</div><!-- Footer Copyright -->
		</div><!-- Footer Copyright container -->
	</div><!-- Footer Copyright -->
</footer>
<!-- Footer -->

<!-- library -->
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.appear.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.easing.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/countdown.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/counter.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.mb.YTPlayer.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.prettyPhoto.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.stellar.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/klient2/js/lib/menu.js"></script> -->

<!-- Revolution Js -->
<script src="<?php echo base_url();?>assets/klient2/revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
<script src="<?php echo base_url();?>assets/klient2/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/theme-rs.js"></script>

<script src="<?php echo base_url();?>assets/klient2/js/lib/modernizr.js"></script>
<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url();?>assets/klient2/js/theme.js"></script>

<!-- Theme Custom -->
<!-- <script src="js/custom.js"></script> -->
</html>

<script>
	$( document ).ready(function() {
		var colors = ['#EEB269', '#DC5653', '#4BB29D', '#2AA1DA', '#FEDE79', '#D0E8F4'];
		var boxes = document.querySelectorAll(".inibox");
		for (i = 0; i < boxes.length; i++) {
			// Pick a random color from the array 'colors'.
			boxes[i].style.backgroundColor = colors[Math.floor(Math.random() * colors.length)];
			boxes[i].style.display = 'inline-block';
		}
	});
</script>
