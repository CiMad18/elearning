<!DOCTYPE html>
<html>
	<head>
		<!-- Basic -->
		<meta charset="utf-8">
		<title>Elearning Deaf Student</title>
		<meta name="keywords" content="Universh - Material Education, Events, News, Learning Centre & Kid School MultiPurpose HTML5 Template" />
		<meta name="description" content="Universh - Material Education, Events, News, Learning Centre & Kid School MultiPurpose HTML5 Template">
		<meta name="author" content="glorythemes.in">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Favicon -->
        <link rel="shortcut icon" href="images/default/favicon.png">
		<!-- Web Fonts  -->
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' 
		rel='stylesheet' type='text/css'>
		<!-- Lib CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/univershicon.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/prettyPhoto.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/menu.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/lib/timeline.css">
		
		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/theme-responsive.css">
		
		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->
		
		<!-- Skins CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/skins/default.css">
		
		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/klient2/css/custom.css">
	</head>
<body>

<!-- Page Loader -->
<div id="pageloader">
	<div class="loader-inner">
		<img src="<?php echo base_url();?>assets/klient2/images/default/preloader.gif" alt="">
	</div>
</div><!-- Page Loader -->

<!-- Back to top -->
<a href="#0" class="cd-top">Top</a>
		
<!-- Page Main -->
<div class="main relative full-screen bg-img bg-cover overlay bg-color heavy" data-background="<?php echo base_url();?>assets/klient2/images/galeri/login-bg.jpg"  data-stellar-background-ratio="0.5">
	<!-- Section -->
	<div class="page-default typo-dark">
		<!-- Container -->
		<div class="container">
			<!-- Row -->
			<div class="row">
				<div class="col-md-offset-4 col-md-4 parent-has-overlay">
					<ul class="template-box box-login text-center">
						<!-- Page Template Logo -->
						<li class="logo-wrap" text-align="center">
								<img width="211" height="40" class="img-responsive" src="<?php echo base_url();?>assets/klient2/images/default/fixsekalilogo.png">
								<p class="slogan">Masuk ke Akunmu</p>
							</a>
						</li><!-- Page Template Logo -->
						<!-- Page Template Content -->
						<li class="template-content">
							<div class="contact-form">
								<!-- Form Begins -->
								<form name="bootstrap-form" action="<?php echo base_url(); ?>login/aksi_login" method="POST" enctype="multipart/form-data">
									<!-- Field 1 -->
									<div class="input-text form-group">
										<input type="text" name="username" class="input-name form-control" placeholder="Username Or Email" />
									</div>
									<!-- Field 2 -->
									<div class="input-email form-group text-left">
										<a class="pull-right" href="#">(Lost Password?)</a>
										<label>Password</label>
										<input type="password" id="password" name ="password" value="" class="form-control" placeholder="Password" />
										<?php if($this->session->flashdata("message")){ ?>
										<br><span style="color:red"><?php echo ($this->session->flashdata("message"))?></span>
										<?php } ?>
									</div>
									<!-- Button -->
									<button class="btn" type="submit">Login</button>
									<a href="<?php echo base_url();?>Register"><button class="btn" type="submit">Sign up Now</button></a>
								</form><!-- Form Ends -->
							</div>	
						</li><!-- Page Template Content -->
					</ul>
				</div><!-- Column -->
			</div><!-- Row -->
		</div><!-- Container -->	
	</div><!-- Page Default -->
</div><!-- Page Main -->

<!-- library -->
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/bootstrapValidator.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.appear.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.easing.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/owl.carousel.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/countdown.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/counter.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.easypiechart.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.mb.YTPlayer.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.prettyPhoto.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/jquery.stellar.min.js"></script>
<script src="<?php echo base_url();?>assets/klient2/js/lib/menu.js"></script>

<script src="<?php echo base_url();?>assets/klient2/js/lib/modernizr.js"></script>
<!-- Theme Base, Components and Settings -->
<script src="<?php echo base_url();?>assets/klient2/js/theme.js"></script>

<!-- Theme Custom -->
<script src="<?php echo base_url();?>assets/klient2/js/custom.js"></script>

</body>
</html>
