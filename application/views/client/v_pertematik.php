<div role="main" class="main">
<!-- Section -->
	<section style="padding-top: 50px; text-align:center;">
		<div class="container">
		    <h3 class="fonttittle">Tema <?= $tema?></h3>
                <div class="row" style="margin-bottom:200px">
                    <div class="col-sm-12">
                    <?php
                    if(count($getvideo) > 0){
                    foreach($getvideo as $gv){
                    $click = "getvideo('".$gv['id_kamus']."','".$gv['kata']."','".$gv['video_kata']."')";
                    $url = $thumbnail_url.preg_replace("/\\.[^.\\s]{3,4}$/", "",$gv['video_kata'] ).".jpeg".'")';
                    echo '
                    <div class="col-md-4">
                        <div class="thumbnail clearfix inibox">
                            <div class="imgarea" style="text-align:center">
                                <img width="100%" controls style="overflow: hidden; padding: 0; float: left;" data-toggle="modal" onclick="'.$click.'")" data-target="#myModal" src="'.$url.'" alt="">
                                <button type="button" style="margin-top:4px" class="btn btn-primary btn-lg" data-toggle="modal" onclick="'.$click.'" data-target="#myModal">
                                '.$gv['kata'].'
                                </button>
                            </div>
                        </div>
                    </div>';
                        }
                    }else{
                        echo '
                        <div class="col-sm-12">
                            <h1>Tidak Ada File </h1>
                        </div>';
                    } 
                    ?>
                    </div>
                </div>
        </div><!-- Container -->
	</section><!-- Section -->
</div><!-- Page Main -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="titlepop"></h4>
		</div>
		<div class="modal-body">
			<div id="videoframe" class="embed-responsive embed-responsive-16by9">
				<video  width="100%" controls style="overflow: hidden; padding: 0; float: left;">
					<source id="videopop" src="" />
				</video>
			</div>
		</div>
    </div>
  </div>
</div>
<script>
function getvideo(id, kata, video) {
	$('#videopop').removeAttr("src").attr("src", '<?= $kamus_url?>'+video);
	$('#titlepop').html(kata);
   	document.getElementById("videoframe").innerHTML = document.getElementById("videoframe").innerHTML;
}
</script>