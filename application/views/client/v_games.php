<section id="content" style="padding-top: 50px;">
  <div class="container">
    <div class="row ">
	  <div class="span12">
          <h3 class="fonttittle">Games</h3>
          <div role="main" class="main">
			<div class="page-default bg-white typo-dark" style="padding-top: 10px;">
				<!-- Container -->
				<div class="container">
					<div class="row">
						<!-- Page Content -->
						<div class="col-md-9">
							<!-- List row -->
							<ul class="row">
								<!-- Event Column -->
								<?php foreach ($data as $data){ ?>
								<li class="col-xs-12 event-list-wrap">
									<!-- Event Wrapper -->
									<div class="row">
										<!-- Event Image Wrapper -->
										<div class="col-sm-5">
											<div class="event-img-wrap">
												<img alt="Event" class="img-responsive" src="<?= $img_path.$data['gambar'] ?>" width="600" height="220">
											</div>
										</div><!-- Event Image Wrapper -->
										<!-- Event Detail Wrapper -->
										<div class="col-sm-7">
											<div class="event-details">
												<h5><a href="event-single-left.html"><?= $data['tema']?></a></h5>
												<ul class="events-meta">
													<li><i class="icon-level-up"></i>Level <?= $data['level']?></li>
												</ul><!-- Event Meta -->
												<div class="progress" data-height="20">
													<div data-percentage="<?= $data['score'] ?>" aria-valuemax="100" aria-valuemin="0" aria-valuenow="60" role="progressbar" data-bg="#ffcc00"  class="progress-bar">
														<?= ($data['score'] > 0 )? $data['score']."%" : '' ?>
													</div>
												</div><!-- Progress Bar -->
												<a href="<?php echo base_url();?>GamesClient/Pilihlevel/<?= $data['tema']?>" class="btn">Mulai</a>
											</div><!-- Event Meta -->
										</div>	<!-- Event details -->
									</div><!-- Event detail Wrapper -->
									<!-- Divider -->
									<hr class="md"/>
								</li><!-- Column -->
								<?php } ?>
							</ul><!-- List row -->
						</div><!-- Column -->
						
					</div><!-- Row -->
				</div><!-- Container -->
			</div><!-- Page Default -->
		</div><!-- Page Main -->
	</div>
  </div><!-- End Container -->
</section>
</div><!-- bg top  -->
<script type="text/javascript" src="<?php echo base_url();?>assets/klient/js/bootstrap.js"></script>
</body>
</html>