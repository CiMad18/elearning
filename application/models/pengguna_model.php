<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengguna_model extends CI_Model {

	public function getalluser()
	{
		//buka koneksi database
		$this->load->database();

		$sql = "SELECT * FROM `pengguna`" ;
		$query = $this->db->query($sql) ;

		$result = $query->result_array();

		return $result;
		//echo '<pre>',print_r($result),'</pre>';die;
    }

    public function adduser($foto_profile)
	{
        $pengguna = $this->input->post('pengguna');
        $namleng = $this->input->post('nama_lengkap');
        $nohp = $this->input->post('no_hp');
        $pass = md5($this->input->post('password'));
        $role = $this->input->post('role');

        //buka koneksi database
        $this->load->database();


		$sql = "INSERT INTO pengguna (nama_pengguna,nama_lengkap,no_hp, password, foto_profile, role)
		VALUES ('$pengguna','$namleng','$nohp','$pass', '$foto_profile','$role');" ;

		$this->db->query($sql) ;
		return ($this->db->query('select last_insert_id() as last_id')->result_array());

        // if($query){
		// 	return $this->db->insert_id();
		// }else {
		// 	return false;
		// }
	}

    public function ambilidpengguna($id)
	{
		$this->load->database();

		$sql = "SELECT * FROM `pengguna` where `id_pengguna`='$id'" ;

		$query = $this->db->query($sql) ;

		$result = $query->result_array();
        
		return $result;
    }
    
    public function edituser($foto_profile = null)
	{
		//tangkap data
        $id_pengguna = $this->input->post('id_pengguna');
        $nampeng = $this->input->post('pengguna');
        $namleng = $this->input->post('nama_lengkap');
        $no_hp = $this->input->post('no_hp');
        $pass = $this->input->post('password');
        $role = $this->input->post('role');
		$sql = "UPDATE pengguna SET nama_pengguna = '$nampeng' ";
		if ($foto_profile != null) {
			$sql = $sql.",foto_profile = '$foto_profile' ";
		}
		if ($namleng != null) {
			$sql = $sql.",nama_lengkap = '$namleng' ";
		}
		if ($no_hp != null) {
			$sql = $sql.",no_hp = '$no_hp' ";
		}
		if ($pass != null) {
			$sql = $sql.",password = '$pass' ";
		}
		if ($pass != null) {
			$sql = $sql.",role = '$role' ";
		}
		$sql = $sql."WHERE id_pengguna = '$id_pengguna' ";
		
		$query = $this->db->query($sql) ;
		return $sql;
	}

	public function deluser(){

        $id = $this->input->post('id');

		$sql = "SELECT * FROM `pengguna` where `id_pengguna`='$id'" ;
		$query = $this->db->query($sql) ;
		$result = $query->result_array();
		$foto = $result[0]['foto_profile'];
		if (count($result) > 0){
			if(unlink("assets/fotoprofile/".$foto)){
				$this->load->database();
				$sql = "DELETE FROM `pengguna` WHERE id_pengguna = '$id' ";
				$query = $this->db->query($sql);
				if($query == 1){
					return "success";
				}else return "gagal";
			}else{
				return "tidak berhasil hapus foto";
			}
		}else{ 
			return "data tidak ada";
		}
	}

	public function ambilrowcount()
	{
		// hitung jumlah baris di
		$sql = "SELECT COUNT(id_pengguna) FROM pengguna";
		// $result = $jumlahBaris->result_array();
		$query = $this->db->query($sql);

		// $result = obj
		$result = $query->result_array();

		// $result = [0] berisi ["COUNT(id_pengguna)"]
		$result = $result[0];

		
		$result = $result["COUNT(id_pengguna)"];

		return $result;
	}
}