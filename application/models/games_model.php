<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Games_model extends CI_Model {

	public function getgamesdata()
	{
		$this->load->database();
		$id_pengguna = $_SESSION['userlog'];
		$sql = "SELECT tema.nama_tema as tema, tema.gambar_tema as gambar, `level`, `score` FROM `games` INNER JOIN `tema` ON games.id_tema=tema.id_tema WHERE id_pengguna='$id_pengguna' 
		and games.id_tema not like '4'
		and games.id_tema not like '5'
		and games.id_tema not like '8' 
		and games.id_tema not like '7'  
		and games.id_tema not like '9'
		and games.id_tema not like '10'" 
		;
		return $this->db->query($sql)->result_array();
	}

	public function getgamesbytema($tema = null)
	{	
		$this->load->database();
		$id_pengguna = $_SESSION['userlog'];
		$sql = "SELECT games.level, games.score FROM `games` INNER JOIN `tema` ON games.id_tema=tema.id_tema WHERE games.id_pengguna='$id_pengguna' and tema.nama_tema='$tema'" ;
		return $this->db->query($sql)->result_array();
	}

	public function getpertanyaan($tema = null,$level = null)
	{
		$this->load->database();
		if($level == 1){
			$sql = "SELECT kata, video_kata FROM `kamus` where `tema`='$tema' ORDER BY RAND()" ;
			$query = $this->db->query($sql) ;
		}else {
			$sql = "SELECT kalimat, video_kalimat FROM `kalimat` where `tema`='$tema' and `jumlah_kata`='$level' ORDER BY RAND()" ;
			$query = $this->db->query($sql) ;
		}

		$result = $query->result_array();

		return $result;
	}

	public function getpilihanjawab($tema = null,$level = null)
	{
		$this->load->database();
		if($level == 1){
			$sql = "SELECT kata, video_kata FROM `kamus` where `tema`!='$tema'" ;
			$query = $this->db->query($sql) ;
		}else {
			$sql = "SELECT kalimat, video_kalimat FROM `kalimat` where `tema`!='$tema' and `jumlah_kata`='$level'" ;
			$query = $this->db->query($sql) ;
		}

		$result = $query->result_array();

		return $result;
	}
	
	public function gettemaid($tema = null)
	{
		$this->load->database();
		$sql = "SELECT id_tema FROM `tema` where nama_tema='$tema'" ;
		$query = $this->db->query($sql) ;
		$result = $query->result_array()[0]['id_tema'];
		return $result;
	}

	public function updatelevel()
	{
		$this->load->database();
        $id_pengguna = $this->input->post('id_pengguna');
		$id_tema = $this->input->post('id_tema');
		$maxlvl = $this->input->post('maxlvl');
		$level = $this->input->post('level');
		$score = $this->input->post('score');
		if($score == '100') {
			if($level == $maxlvl){
				if ($level < 3){
					$level = $level + 1 ;
					$score = '0'; 
				} else {
					$level = 3;
				}
			}
		}
		$sql = "UPDATE games SET `level` = '$level', `score` = '$score' where id_pengguna = '$id_pengguna' and id_tema = '$id_tema' ";
		$this->db->query($sql) ;
		return $sql;
	}

	public function addgamedata($id_pengguna = null, $id_tema = null)
	{
        $this->load->database();
		$sql = "INSERT INTO `games`(`id_pengguna`, `id_tema`, `level`, `score`) VALUES ('$id_pengguna','$id_tema','1', '0')" ;
		$this->db->query($sql) ;
	}

}



