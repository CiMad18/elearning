<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kamus_model extends CI_Model {

	public function getallkata()
	{

		//buka koneksi database
		$this->load->database();

		// SELECT * FROM Customers
		// ORDER BY CustomerName ASC

		$sql = "SELECT id_kamus, kata, video_kata, tema, LEFT(kata, 1) as huruf FROM `kamus` ORDER BY kata ASC" ;
		$query = $this->db->query($sql) ;

		$result = $query->result_array();

		return $result;

		//echo '<pre>',print_r($result),'</pre>';die;
	}
	
	public function getkatabytema($tema = null)
	{
		$this->load->database();

		$sql = "SELECT id_kamus, kata, video_kata, tema, LEFT(kata, 1) as huruf FROM `kamus` where tema='$tema'" ;
		$query = $this->db->query($sql) ;

		$result = $query->result_array();

		return $result;
	}
    
    public function gettema()
	{

		//buka koneksi database
		$this->load->database();

		$sql = "SELECT * FROM `tema`" ;
		$query = $this->db->query($sql) ;

		$result = $query->result_array();

		return $result;

		//echo '<pre>',print_r($result),'</pre>';die;
    }

    public function addkamus($file){
        $kosakata = $this->input->post('kosakata');
		$tema = $this->input->post('tema');

        //buka koneksi database
        $this->load->database();

		$sql = "INSERT INTO kamus (kata,video_kata,tema)VALUES ('$kosakata','$file','$tema')" ;

		$query = $this->db->query($sql) ;
        
		return $query;
	}

	public function ambilkata($id = null)
	{
		$this->load->database();

		$sql = "SELECT * FROM `kamus` where `id_kamus`='$id'" ;

		$query = $this->db->query($sql) ;

		$result = $query->result_array();
        
		return $result;
	}

	public function getkategori()
	{
		$this->load->database();

		$sql = "SELECT LEFT(kata, 1) as huruf FROM `kamus`" ;

		$query = $this->db->query($sql) ;

		$result = $query->result_array();
        
		return $result;
	}

	public function setpage($kategori = null, $page = null)
	{
		$this->load->database();
		$offset = ($page * 12) - 12;
		if($kategori == 'all') $sql = "SELECT * FROM `kamus` ORDER BY kata ASC LIMIT $offset, 12" ;
		else $sql = "SELECT * FROM `kamus` where `kata` LIKE '$kategori%' ORDER BY kata ASC LIMIT $offset, 12" ;

		$query = $this->db->query($sql) ;

		$result = $query->result();
        
		return $result;
	}

	public function getSearch($key = null)
	{
		$this->load->database();
		$sql = "SELECT * FROM `kamus` WHERE `kata` LIKE '$key%' ORDER BY kata ASC" ;

		$query = $this->db->query($sql) ;

		$result = $query->result();
        
		return $result;
	}

	public function totalpage($kategori = null)
	{
		$this->load->database();
		if($kategori == null || $kategori == 'all') $sql = "SELECT count(`kata`) as jumlah FROM `kamus`" ;
		else $sql = "SELECT count(`kata`) as jumlah FROM `kamus` where LEFT(kata, 1) = '$kategori'";
		$result = $this->db->query($sql)->result_array()[0]['jumlah'] ;
        $total_page = ceil($result / 12);
		return $total_page;
	}

	public function getvideoid()
	{
		$this->load->database();

		$sql = "SELECT * FROM `kamus` where `id_kamus` " ;

		$query = $this->db->query($sql) ;

		$result = $query->result_array();
        
		return $result;
	}
	
	public function editkata($file = null)
	{
		//tangkap data
		$id = $this->input->post('id_kamus');
		$kosakata = $this->input->post('kosakata');
		$tema = $this->input->post('tema');
		$sql = "UPDATE kamus SET kata = '$kosakata' ";
		if ($file != null) {
			$sql = $sql.",video_kata = '$file' ";
		}
		if ($tema != null) {
			$sql = $sql.",tema = '$tema' ";
		}
		
		$sql = $sql."WHERE id_kamus = '$id' ";
		
		$query = $this->db->query($sql) ;
		return $sql;
	}
	
	public function delkata(){

		$id = $this->input->post('id');

		$sql = "SELECT * FROM `kamus` where `id_kamus`='$id'" ;
		$query = $this->db->query($sql) ;
		$result = $query->result_array();
		$kamus = $result[0]['video_kata'];
		if (count($result) > 0){
			if(unlink("assets/kamus/".$kamus)){
				$this->load->database();
				$sql = "DELETE FROM `kamus` WHERE id_kamus = '$id' ";
				$query = $this->db->query($sql);
				if($query == 1){
					return "success"; 
				}else return "gagal";
			}else{
				return "tidak berhasil hapus video";
			}
		}else{
			return "data tidak ada";
		}

	}
	
	public function get_paging($limit, $start)
	{
		$query = $this->db->get('kamus', $limit, $start);
		return $query;
	}

	// public function paging(){

	// }

	public function get_count() {
    return $this->db->count_all($this->table);
	}

	public function get_kamus($limit, $start) {
    $this->db->limit($limit, $start);
	$this->db->select('id_kamus, kata, video_kata, tema, LEFT(kata, 1) as huruf');
    $query = $this->db->get('kamus');
    return $query->result_array();
	}

	public function ambilrowcount()
	{
		// hitung jumlah baris di
		$sql = "SELECT COUNT(id_kamus) FROM kamus";
		// $result = $jumlahBaris->result_array();
		$query = $this->db->query($sql);

		// $result = obj
		$result = $query->result_array();

		// $result = [0] berisi ["COUNT(id_kamus)"]
		$result = $result[0];

		
		$result = $result["COUNT(id_kamus)"];

		return $result;
	}


}



