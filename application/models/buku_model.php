<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku_model extends CI_Model {

	public function getallbuku()
	{

		//buka koneksi database
		$this->load->database();

		$sql = "SELECT * FROM `buku`" ;
		$query = $this->db->query($sql) ;

		$result = $query->result_array();

		return $result;

		//echo '<pre>',print_r($result),'</pre>';die;
	}
	
	public function addbuku($fotobuku,$filebuku)
	{
		$nambuk = $this->input->post('nambuk');

        //buka koneksi database
        $this->load->database();

		$sql = "INSERT INTO buku (nama_buku,foto_buku,file_buku)VALUES ('$nambuk','$fotobuku','$filebuku')" ;

		$query = $this->db->query($sql) ;
        
		return $query;
	}

	public function ambilidbuku($id)
	{
		$this->load->database();

		$sql = "SELECT * FROM `buku` where `id_buku`='$id'" ;

		$query = $this->db->query($sql) ;

		$result = $query->result_array();
        
		return $result;
	}

	public function editbuku($fotobuku = null,$filebuku = null)
	{
		//tangkap data
		$id_buku = $this->input->post('id_buku');
		$namabuku = $this->input->post('nambuk');
		$sql = "UPDATE buku SET nama_buku = '$namabuku' ";
		if ($filebuku != null) {
			$sql = $sql.",file_buku = '$filebuku' ";
		}
		if ($fotobuku != null) {
			$sql = $sql.",foto_buku = '$fotobuku' ";
		}
		$sql = $sql."WHERE id_buku = '$id_buku' ";
		
		$query = $this->db->query($sql) ;
		return $sql;
	}

	public function delbuku(){

		$id = $this->input->post('id');

		$sql = "SELECT * FROM `buku` where `id_buku`='$id'" ;
		$query = $this->db->query($sql) ;
		$result = $query->result_array();
		$fotobuku = $result[0]['foto_buku'];
		$filebuku = $result[0]['file_buku'];
		if (count($result) > 0){
			if(unlink("assets/fotobuku/".$fotobuku) && unlink("assets/filebuku/".$filebuku)){
				$this->load->database();
				$sql = "DELETE FROM `buku` WHERE id_buku = '$id' ";
				$query = $this->db->query($sql);
				if($query == 1){
					return "success";
				}else return "gagal";
			}else{
				return "tidak berhasil hapus kalimat";
			}
		}else{
			return "data tidak ada";
		}

	}

	public function ambilrowcount()
	{
		// hitung jumlah baris di
		$sql = "SELECT COUNT(id_buku) FROM buku";
		// $result = $jumlahBaris->result_array();
		$query = $this->db->query($sql);
		// $result = obj
		$result = $query->result_array();
		// $result = [0] berisi ["COUNT(id_buku)"]
		$result = $result[0];
		$result = $result["COUNT(id_buku)"];
		return $result;
	}

}	

// 	public function getoneasesi($id)
// 	{

// 		$sql = "SELECT * FROM asesi WHERE id_asesi ='$id'" ;
// 		$query = $this->db->query($sql) ;

// 		$result = $query->result_array();

// 		return $result;

// 	}
// 	public function addasesor2()
// 	{

// 		$sql = "SELECT * FROM asesor" ;
// 		$query = $this->db->query($sql) ;

// 		$result = $query->result_array();

// 		return $result;

// 	}	
// 	public function ambilrowcount()
// 	{
// 		// hitung jumlah baris di
// 		$sql = "SELECT COUNT(*) FROM asesi ai join asesor ae on ai.id_asesor=ae.id_asesor";
// 		// $result = $jumlahBaris->result_array();
// 		$query = $this->db->query($sql) ;
// 		$result = $query->result_array();
// 		$result = $result[0];
// 		$result = $result["COUNT(*)"];
// 		return $result;
// 	}

// 		public function updateasesi(){
// 		//tangkap data
// 		$nama = $this->input->post('nama');
// 		$id = $this->input->post('id_asesi');
// 		$nim = $this->input->post('nim');
// 		$prodi = $this->input->post('prodi');
// 		$jurusan = $this->input->post('jurusan');
// 		$email = $this->input->post('email');
// 		$asesor = $this->input->post('asesor');

// 		$sql = "UPDATE asesi SET
// 				nama_asesi = '$nama',
// 				nim = '$nim',
// 				prodi = '$prodi',
// 				jurusan = '$jurusan',
// 				email = '$email',
// 				id_asesor = '$asesor'
// 				WHERE id_asesi='$id'";
		
// 		$query = $this->db->query($sql) ;
// 		return;
// 		} 


