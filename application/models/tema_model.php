<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tema_model extends CI_Model {

	public function getalltema()
	{
		$this->load->database();

		$sql = "SELECT * FROM `tema` where nama_tema not like 'general'" ;
		$query = $this->db->query($sql) ;

		return $result = $query->result_array();
	}
	
	public function gettemabyid($id=null)
	{
		$this->load->database();

		$sql = "SELECT * FROM `tema` where id_tema='$id'" ;
		$query = $this->db->query($sql) ;

		$result = $query->result()[0];

		return $result;
	}

    public function addtema($gambar_tema)
	{
        $namatema = $this->input->post('nama_tema');
        $this->load->database();

		$sql = "INSERT INTO tema (nama_tema, gambar_tema)VALUES ('$namatema','$gambar_tema')" ;

		$query = $this->db->query($sql) ;
        
		return $query;
	}

    public function ambilidtema($id)
	{
		$this->load->database();

		$sql = "SELECT * FROM `tema` where `id_tema`='$id'" ;

		$query = $this->db->query($sql) ;

		$result = $query->result_array();
        
		return $result;
    }
    
    public function edittema($gambar_tema = null)
	{
		//tangkap data
        $id_tema = $this->input->post('id_tema');
        $namatema = $this->input->post('nama_tema');
		$sql = "UPDATE tema SET nama_tema = '$namatema' ";
		if ($gambar_tema != null) {
			$sql = $sql.",gambar_tema = '$gambar_tema' ";
		}
		$sql = $sql."WHERE id_tema = '$id_tema' ";
		
		$query = $this->db->query($sql) ;
		return $sql;
	}
    
	public function deltema(){

        $id = $this->input->post('id');

		$sql = "SELECT * FROM `tema` where `id_tema`='$id'" ;
		$query = $this->db->query($sql) ;
		$result = $query->result_array();
		$foto = $result[0]['gambar_tema'];
		if (count($result) > 0){
			if(unlink("assets/gambartema/".$foto)){
				$this->load->database();
				$sql = "DELETE FROM `tema` WHERE id_tema = '$id' ";
				$query = $this->db->query($sql);
				if($query == 1){
					return "success";
				}else return "gagal";
			}else{
				return "tidak berhasil hapus gambar";
			}
		}else{
			return "data tidak ada";
		}
	}
}