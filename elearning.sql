-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Feb 2021 pada 10.22
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elearning`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `id_buku` int(11) NOT NULL,
  `nama_buku` varchar(100) NOT NULL,
  `foto_buku` varchar(20) NOT NULL,
  `file_buku` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`id_buku`, `nama_buku`, `foto_buku`, `file_buku`) VALUES
(57, 'Kelas 1 Tema 2', 'sd 1 tema 2.png', 'BS Tunarungu 1 Tema 2_compressed.pdf'),
(58, 'Kelas 1 Tema 3', 'sd 1 tema 3.png', 'BS Tunarungu 1 Tema 3_compressed.pdf'),
(59, 'Kelas 1 Tema 4', 'sd 1 tema 4.png', 'BS Tunarungu 1 Tema 4_compressed.pdf'),
(60, 'Kelas 2 Tema 1', 'sd 2 tema 1.png', 'BS Tunarungu 2 Tema 1_compressed.pdf'),
(61, 'Kelas 2 Tema 5', 'sd 2 tema 5.png', 'BS Tunarungu 2 Tema 5_compressed.pdf'),
(62, 'Kelas 2 Tema 6', 'sd 2 tema 6.png', 'BS Tunarungu 2 Tema 6_compressed.pdf'),
(63, 'Kelas 2 Tema 7', 'sd 2 tema 7.png', 'BS Tunarungu 2 Tema 7_compressed.pdf'),
(64, 'Kelas 2 Tema 7', 'sd 2 tema 7.png', 'BS Tunarungu 2 Tema 7_compressed.pdf'),
(65, 'Kelas 2 Tema 8', 'sd 2 tema 8.png', 'BS Tunarungu 2 Tema 8_compressed.pdf'),
(66, 'Kelas 3 Tema 1', 'sd 3 tema 1.png', 'BS Tunarungu 3 Tema 1_compressed.pdf'),
(67, 'Kelas 3 Tema 2', 'sd 3 tema 2.png', 'BS Tunarungu 3 Tema 2_compressed.pdf'),
(68, 'Kelas 3 Tema 3', 'sd 3 tema 3.png', 'BS Tunarungu 3 Tema 3_compressed.pdf'),
(69, 'Kelas 3 Tema 4 ', 'sd 3 tema 4.png', 'BS Tunarungu 3 Tema 4_compressed.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `games`
--

CREATE TABLE `games` (
  `id` int(11) NOT NULL,
  `nama_pengguna` varchar(255) NOT NULL,
  `tema` varchar(255) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `games`
--

INSERT INTO `games` (`id`, `nama_pengguna`, `tema`, `level`) VALUES
(12, 'cimadeee', 'Aktifitas', 3),
(17, 'adinda', 'Aktifitas', 1),
(18, 'adinda', 'images', 1),
(19, 'cimadeee', 'Benda-benda_di_sekitar', 1),
(20, 'cimadeee', 'Anggota_keluarga_dan_kenalan', 1),
(21, 'cimadeee', 'Hari_besar_dan_hari_libur', 1),
(22, 'adinda', 'Benda-benda_di_sekitar', 1),
(23, 'cimadeee', 'images', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kalimat`
--

CREATE TABLE `kalimat` (
  `id_kalimat` int(11) NOT NULL,
  `kalimat` varchar(100) NOT NULL,
  `video_kalimat` varchar(100) NOT NULL,
  `tema` varchar(10) NOT NULL,
  `jumlah_kata` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kalimat`
--

INSERT INTO `kalimat` (`id_kalimat`, `kalimat`, `video_kalimat`, `tema`, `jumlah_kata`) VALUES
(32, 'Baca tulis', 'baca_tulis.MP4', '2', 2),
(33, 'Beli makan', 'beli_makan.MP4', '2', 2),
(34, 'Cuci baju', 'cuci_baju.MP4', '2', 2),
(35, 'Goreng nasi', 'goreng_nasi.MP4', '2', 2),
(36, 'Ingin melihat', 'ingin_melihat.MP4', '2', 2),
(37, 'Masuk lubang', 'masuk_lubang.MP4', '2', 2),
(38, 'Pakai baju', 'pakai_baju.MP4', '2', 2),
(39, 'Tabrak lari', 'tabrak_lari.MP4', '2', 2),
(40, 'Tarik ulur', 'tarik_ulur.MP4', '2', 2),
(41, 'Minta tolong', 'minta_tolong.MP4', '2', 2),
(42, 'Antar ibu pergi', 'antar_ibu_pergi.MP4', '2', 3),
(43, 'Bapak baca buku', 'bapak_baca_buku.MP4', '2', 3),
(44, 'Ibu pergi pasar', 'ibu_pergi_pasar.MP4', '2', 3),
(45, 'Kakak minta tolong', 'kakak_minta_tolong.MP4', '2', 3),
(46, 'Kamu lihat saya', 'kamu_lihat_saya.MP4', '2', 3),
(47, 'Lupa minta uang ', 'lupa_minta_uang.MP4', '2', 3),
(48, 'Pakai baju baru', 'pakai_baju_baru.MP4', '2', 3),
(49, 'Saya cinta kamu', 'saya_cinta_kamu.MP4', '2', 3),
(50, 'Teman saya jatuh', 'teman_saya_jatuh.MP4', '2', 3),
(51, 'Tinggal di rumah baru', 'tinggal_dirumah_baru.MP4', '2', 3),
(52, 'Bantal guling', 'bantal_guling.MP4', '3', 2),
(53, 'Bel berbunyi', 'bel_berbunyi.MP4', '3', 2),
(55, 'Kertas surat', 'kertas_surat.MP4', '3', 2),
(56, 'Kapur tulis', 'kapur_tulis.MP4', '3', 2),
(57, 'Kertas surat', 'kertas_surat.MP4', '3', 2),
(58, 'Lampu rumah', 'lampu_rumah.MP4', '3', 2),
(59, 'Menyapu lantai', 'menyapu_lantai.MP4', '3', 2),
(60, 'Piring bersih', 'piring_bersih.MP4', '3', 2),
(61, 'Telepon bunyi', 'telepon_bunyi.MP4', '3', 2),
(62, 'Tiang bendera ', 'tiang_bendera.MP4', '3', 2),
(63, 'Bel sekolah berbunyi', 'bel_sekolah_berbunyi.MP4', '3', 3),
(64, 'Gelas itu pecah', 'gelas_itu_pecah.MP4', '3', 3),
(65, 'Genteng itu berlubang', 'genteng_itu_berlubang.MP4', '3', 3),
(66, 'Jendela rumah terbuka ', 'jendela_rumah_terbuka.MP4', '3', 3),
(67, 'Lampu rumah terang', 'lampu_rumah_terang.MP4', '3', 3),
(68, 'Lemari diisi baju ', 'lemari_diisi_baju.MP4', '3', 3),
(69, 'Papan tulis panjang', 'papan_tulis_panjang.MP4', '3', 3),
(70, 'Pensil untuk menulis', 'pensil_untuk_menulis.MP4', '3', 3),
(71, 'Piring sudah dicuci', 'piring_sudah_dicuci.MP4', '3', 3),
(72, 'Amplop diisi uang', 'amplop_diisi_uang.MP4', '3', 3),
(73, 'Agama islam ', 'agama_islam.MP4', '6', 2),
(74, 'Agama kristen', 'agama_kristen.MP4', '6', 2),
(75, 'Al-Quran kitabku', 'alquran_kitabku.MP4', '6', 2),
(76, 'Azan maghrib', 'azan_maghrib.MP4', '6', 2),
(77, 'Hari libur ', 'hari_libur.MP4', '6', 2),
(78, 'Hari natal', 'hari_natal.MP4', '6', 2),
(79, 'Hari waisak ', 'hari_waisak.MP4', '6', 2),
(80, 'Kitab injil ', 'kitab_injil.MP4', '6', 2),
(81, 'Lebaran idulfitri', 'lebaran_idulfitri.MP4', '6', 2),
(82, 'Shalat maghrib ', 'shalat_madgrib.MP4', '6', 2),
(83, 'Baca quran bersama ', 'baca_quran_bersama.MP4', '6', 3),
(84, 'Ibadah bersama keluarga ', 'ibadah_bersama_keluarga.MP4', '6', 3),
(87, 'Jalan-jalan di kampung', 'jalan_jalan_dikampung.MP4', '6', 3),
(88, 'Kami pulang kampung', 'kami_pulang_kampung.MP4', '6', 3),
(89, 'Keluarga lebaran di kampung', 'keluarga_lebaran_dikampung.MP4', '6', 3),
(90, 'Liburan di kampung', 'liburan_di_kampung.MP4', '6', 3),
(91, 'Puasa bulan ramadhan', 'puasa_bulan_ramadhan.MP4', '6', 3),
(92, 'Selamat bulan puasa', 'selamat_bulan_puasa.MP4', '6', 3),
(93, 'Shalat ashar di rumah', 'shalat_ashar_dirumah.MP4', '6', 3),
(94, 'Shalat zuhur di masjid', 'shalat_zuhur_dimasjid.MP4', '6', 3),
(96, 'Anak kembar', 'anak_kembar.MP4', '16', 2),
(97, 'Bapak ibu', 'bapak_ibu.MP4', '16', 2),
(98, 'Calon jodoh', 'calon_jodoh.MP4', '16', 2),
(99, 'Mertua adik', 'mertua_adik.MP4', '16', 2),
(100, 'Pacar baru', 'pacar_baru.MP4', '16', 2),
(101, 'Paman bibi', 'paman_bibi.MP4', '16', 2),
(102, 'Pertemuan keluarga', 'pertemuan_keluarga.MP4', '16', 2),
(103, 'Putra tetangga', 'putra_tetangga.MP4', '16', 2),
(104, 'Rencana menikah', 'rencana_menikah.MP4', '16', 2),
(105, 'Suami istri', 'suami_istri.MP4', '16', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kamus`
--

CREATE TABLE `kamus` (
  `id_kamus` int(11) NOT NULL,
  `kata` varchar(50) NOT NULL,
  `video_kata` varchar(50) NOT NULL,
  `tema` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kamus`
--

INSERT INTO `kamus` (`id_kamus`, `kata`, `video_kata`, `tema`) VALUES
(43, 'Abad', 'anak_kembar.MP4', '1'),
(44, 'Abadi', 'abadi.MP4', '1'),
(45, 'Abang', 'abang.MP4', '1'),
(46, 'Abdi', 'abdi.MP4', '1'),
(47, 'Aben', 'aben.MP4', '1'),
(48, 'Abjad', 'abjad.MP4', '1'),
(49, 'Abortus', 'abortus_.MP4', '1'),
(50, 'Absen', 'absen.MP4', '1'),
(51, 'B', 'B.MP4', '1'),
(52, 'Bab', 'bab.MP4', '1'),
(53, 'Babak', 'babak.MP4', '1'),
(54, 'Babi', 'babi.MP4', '1'),
(55, 'Baca', 'baca.MP4', '1'),
(56, 'Badak', 'badak.MP4', '1'),
(57, 'Badan', 'badan.MP4', '1'),
(58, 'Bagai', 'bagai.MP4', '1'),
(59, 'Bagaimana', 'bagaimana.MP4', '1'),
(60, 'Bagi', 'bagi.MP4', '1'),
(61, 'Bagus', 'bagus.MP4', '1'),
(62, 'C', 'C.MP4', '1'),
(63, 'Cacat', 'cacat.MP4', '1'),
(64, 'Cacing', 'cacing.MP4', '1'),
(65, 'Cair ', 'cair.MP4', '1'),
(66, 'Cakap', 'cakap.MP4', '1'),
(67, 'Cakar', 'cakar.MP4', '1'),
(68, 'Cakup', 'cakup.MP4', '1'),
(69, 'Calon', 'calon.MP4', '1'),
(70, 'Camat', 'camat.MP4', '1'),
(71, 'Cambuk', 'cambuk.MP4', '1'),
(72, 'Campak', 'campak.MP4', '1'),
(73, 'D', 'D.MP4', '1'),
(74, 'Dada', 'dada.MP4', '1'),
(75, 'Dadu', 'dadu.MP4', '1'),
(76, 'Daerah', 'daerah.MP4', '1'),
(78, 'Daftar', 'daftar.MP4', '1'),
(79, 'Dagang', 'dagang.MP4', '1'),
(80, 'Daging', 'daging.MP4', '1'),
(81, 'Dahan', 'dahan.MP4', '1'),
(82, 'Dahulu', 'dahulu.MP4', '1'),
(83, 'Daki', 'daki.MP4', '1'),
(84, 'Dalam', 'dalam.MP4', '1'),
(85, 'E', 'E.MP4', '1'),
(86, 'Ecer', 'ecer.MP4', '1'),
(87, 'Eja', 'eja.MP4', '1'),
(88, 'Ekor', 'ekor.MP4', '1'),
(89, 'Elok', 'elok.MP4', '1'),
(90, 'Emas', 'emas.MP4', '1'),
(91, 'Ember', 'ember.MP4', '1'),
(92, 'Empang', 'empang.MP4', '1'),
(93, 'Empat', 'empat.MP4', '1'),
(95, 'Enak', 'enak.MP4', '1'),
(96, 'F', 'F.MP4', '1'),
(97, 'Fajar', 'fajar.MP4', '1'),
(98, 'Famili', 'famili.MP4', '1'),
(99, 'Februari', 'februari.MP4', '1'),
(100, 'Festival', 'festival.MP4', '1'),
(101, 'Film', 'film.MP4', '1'),
(102, 'Firman', 'firman.MP4', '1'),
(103, 'Fitnah', 'fitnah.MP4', '1'),
(104, 'Foto', 'foto.MP4', '1'),
(105, 'G', 'G.MP4', '1'),
(106, 'Gabung', 'gabung.MP4', '1'),
(107, 'H', 'H.MP4', '1'),
(108, 'I', 'I.MP4', '1'),
(109, 'Ia', 'ia.MP4', '1'),
(110, 'J', 'J.MP4', '1'),
(111, 'Jadi', 'jadi.MP4', '1'),
(112, 'K', 'K.MP4', '1'),
(113, 'Kabar', 'kabar.MP4', '1'),
(114, 'L', 'L.MP4', '1'),
(115, 'Labuh', 'labuh.MP4', '1'),
(116, 'M', 'M.MP4', '1'),
(117, 'Maaf', 'maaf.MP4', '1'),
(118, 'N', 'N.MP4', '1'),
(119, 'Nabi', 'nabi.MP4', '1'),
(120, 'O', 'O.MP4', '1'),
(121, 'Obat', 'obat.MP4', '1'),
(122, 'P', 'P.MP4', '1'),
(123, 'Pabrik', 'pabrik.MP4', '1'),
(124, 'Q', 'Q.MP4', '1'),
(125, 'Quran', 'quran.MP4', '1'),
(126, 'R', 'R.MP4', '1'),
(127, 'Raba', 'raba.MP4', '1'),
(128, 'S', 'S.MP4', '1'),
(129, 'Sabar', 'sabar.MP4', '1'),
(130, 'T', 'T.MP4', '1'),
(131, 'Taat', 'taat.MP4', '1'),
(132, 'U', 'U.MP4', '1'),
(133, 'Uang', 'uang.MP4', '1'),
(134, 'V', 'V.MP4', '1'),
(135, 'Vanili', 'vanili.MP4', '1'),
(136, 'W', 'W.MP4', '1'),
(137, 'Wabah', 'wabah.MP4', '1'),
(138, 'X', 'X.MP4', '1'),
(139, 'Y', 'Y.MP4', '1'),
(140, 'Ya', 'ya.MP4', '1'),
(141, 'Z', 'Z.MP4', '1'),
(143, 'Ajak', 'ajak.MP4', '2'),
(144, 'Ambil', 'ambil.MP4', '2'),
(145, 'Angkat', 'angkat.MP4', '2'),
(146, 'Antar', 'antar.MP4', '2'),
(147, 'Baca', 'baca.MP4', '2'),
(148, 'Bangkit', 'bangkit.MP4', '2'),
(149, 'Bayar', 'bayar.MP4', '2'),
(150, 'Beli', 'beli.MP4', '2'),
(151, 'Bentang ', 'bentang.MP4', '2'),
(152, 'Berangkat ', 'berangkat.MP4', '2'),
(153, 'Almari', 'almari.MP4', '3'),
(154, 'Amplop', 'amplop.MP4', '3'),
(155, 'Atap', 'atap.MP4', '3'),
(156, 'Bantal', 'bantal.MP4', '3'),
(157, 'Batu', 'batu.MP4', '3'),
(158, 'Bel', 'bel.MP4', '3'),
(159, 'Benang', 'benang.MP4', '3'),
(160, 'Bendera', 'bendera.MP4', '3'),
(161, 'Bola', 'bola.MP4', '3'),
(162, 'Buku', 'buku.MP4', '3'),
(163, 'Satu', 'satu.MP4', '4'),
(164, 'Dua', 'dua.MP4', '4'),
(165, 'Tiga', 'tiga.MP4', '4'),
(166, 'Empat', 'empat.MP4', '4'),
(167, 'Lima', 'lima.MP4', '4'),
(168, 'Enam', 'enam.MP4', '4'),
(169, 'Tujuh', 'tujuh.MP4', '4'),
(170, 'Delapan', 'delapan.MP4', '4'),
(171, 'Sembilan', 'sembilan.MP4', '4'),
(172, 'Sepuluh', 'sepuluh.MP4', '4'),
(179, 'taufiq rahman', 'calon_jodoh.MP4', '1'),
(180, 'test', 'mertua_adik.MP4', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengguna`
--

CREATE TABLE `pengguna` (
  `id_pengguna` int(11) NOT NULL,
  `nama_pengguna` varchar(50) NOT NULL,
  `nama_lengkap` varchar(200) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `foto_profile` varchar(50) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengguna`
--

INSERT INTO `pengguna` (`id_pengguna`, `nama_pengguna`, `nama_lengkap`, `no_hp`, `password`, `foto_profile`, `role`) VALUES
(34, 'cimadeee', 'Andi Cici', '082348480295', 'hahahaha', 'cici.jpg', 'Admin'),
(35, 'adinda', 'Adinda', '0852342648434', '123456', 'defaultsiswa.jpg', 'User'),
(38, 'Sirham', 'Muhammad Sirham', '08532452667362', '123456', 'defaultsiswa.jpg', 'User');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tema`
--

CREATE TABLE `tema` (
  `id_tema` int(11) NOT NULL,
  `nama_tema` varchar(100) NOT NULL,
  `gambar_tema` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tema`
--

INSERT INTO `tema` (`id_tema`, `nama_tema`, `gambar_tema`) VALUES
(1, 'general', 'general.jpg'),
(2, 'aktifitas', 'aktifitas.jpg'),
(3, 'benda-benda_di_sekitar', 'bendasekitar.jpg'),
(4, 'bilangan', 'bilangan.jpg'),
(5, 'buah', 'buah.jpg'),
(6, 'hari_besar_dan_hari_libur', 'hari.jpg'),
(7, 'hewan', 'hewan.jpg'),
(8, 'imbuhan', 'imbuhan.jpg'),
(9, 'pekerjaan', 'pekerjaan.jpg'),
(10, 'waktu', 'waktu.jpg'),
(16, 'Anggota_keluarga_dan_kenalan', 'keluarga.jpg');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indeks untuk tabel `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kalimat`
--
ALTER TABLE `kalimat`
  ADD PRIMARY KEY (`id_kalimat`);

--
-- Indeks untuk tabel `kamus`
--
ALTER TABLE `kamus`
  ADD PRIMARY KEY (`id_kamus`);

--
-- Indeks untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_pengguna`);

--
-- Indeks untuk tabel `tema`
--
ALTER TABLE `tema`
  ADD PRIMARY KEY (`id_tema`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT untuk tabel `games`
--
ALTER TABLE `games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `kalimat`
--
ALTER TABLE `kalimat`
  MODIFY `id_kalimat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT untuk tabel `kamus`
--
ALTER TABLE `kamus`
  MODIFY `id_kamus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=181;

--
-- AUTO_INCREMENT untuk tabel `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_pengguna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `tema`
--
ALTER TABLE `tema`
  MODIFY `id_tema` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
